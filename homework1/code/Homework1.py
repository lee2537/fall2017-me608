import os
import sys
import numpy as np
import scipy as sp
import scipy.sparse.linalg as splinalg
from scipy.interpolate import griddata
from scipy.sparse import csr_matrix
import pylab as plt
from pdb import set_trace as keyboard
from matplotlib import rc as matplotlibrc
from matplotlib import rc as matplotlibrc
import matplotlib.pyplot as plt
import matplotlib as mplt
import binascii
import numpy as N
import umesh_reader
from collections import Counter
from matplotlib.pyplot import figure, show
import numpy
from numpy import ma
import math
from sympy import Symbol, solve
import time
import matplotlib.lines as mlines

##############################################################
# Jeongmin's Homework 1                                      #
# Thanks to Prof. Scalo for all your advices for everything  #
#                                                            #
# Mesh file list                                             #
# 1. Mesh_A_ncv_100.msh                                      #
# 2. Mesh_B_ncv_500.msh                                      #
# 3. Mesh_C_ncv_1500.msh                                     #
# 4. Mesh_D_ncv_10000.msh                                    #
#                                                            #
##############################################################

Problem1 = True
Problem2 = False
Problem3 = False

# Display the contour of the computaional domain

if Problem1 :
    icemcfd_project_folder = './'
    file_vec = ['validation_v2.msh'] # ['Mesh_A_ncv_100.msh','Mesh_B_ncv_500.msh','Mesh_C_ncv_1500.msh','Mesh_D_ncv_10000.msh']
    
    for filename in file_vec:
    
    
        mshfile_fullpath = icemcfd_project_folder + filename

        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)

    #####################################################
    ########## Plot Grid Labels / Connectivity ##########
    #####################################################

        fig_width = 30
        fig_height = 17
        textFontSize   = 15
        gcafontSize    = 32
        lineWidth      = 2

        Plot_Node_Labels = True
        Plot_Face_Labels = True
        Plot_CV_Labels   = True

        mgplx = 0.05*np.abs(max(xy_no[:,0])-min(xy_no[:,0]))
        mgply = 0.05*np.abs(max(xy_no[:,1])-min(xy_no[:,1]))

        fig = plt.figure(0,figsize=(fig_width,fig_height))
        ax = fig.add_subplot(111)
        ax.plot(xy_no[:,0],xy_no[:,1],'o',markersize=8,markerfacecolor='k')

        node_color = 'k'
        centroid_color = 'r'

        for inos_of_fa in noofa:
            ax.plot(xy_no[inos_of_fa,0], xy_no[inos_of_fa,1], 'k-', linewidth = lineWidth)

        ax.axis('equal')

        plt.axis([-0.7, 0.7, -0.3, 1.2])
        plt.axis('off')

        ax.arrow(-0.2, -0.2, 0.5, 0, head_width=0.05, head_length=0.05, fc='k', ec='k')
        ax.arrow(-0.2, -0.2, 0, 0.5, head_width=0.05, head_length=0.05, fc='k', ec='k')
        ax.text(0.4, -0.2, 'x', size=35, ha='center', va='center')
        ax.text(-0.2, 0.4, 'y', size=35, ha='center', va='center')

        figure_folder = "../report/"
        figure_name = filename.split('.')[0] +'_GRID GENERATION' +'.png'
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.savefig(figure_file_path)
        plt.close()

##############################################################################
# Problem 2
##############################################################################

if Problem2 :
    icemcfd_project_folder = './'
    file_vec = ['Mesh_A_ncv_100.msh','Mesh_B_ncv_500.msh','Mesh_C_ncv_1500.msh','Mesh_D_ncv_10000.msh']
    
    for filename in file_vec:
    
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(filename,node_reordering=True)
    
        tt = time.time()

    # Initialization, 0 K everywhere
        node_t = 0.*np.ones(len(xy_no))
        cv_t = 0.*np.ones(len(xy_cv))
    
    # find the faces connecting to the boundary
        ff_hot = []; ff_cold = [];
        ff_hot = np.where(np.array(partofa) == 'HOT')
        ff_cold = np.where(np.array(partofa) == 'COLD')
    
    # find the nodes connecting to the faces
        nn_hot = noofa[ff_hot]
        nn_cold = noofa[ff_cold]
    
    # find the cells connecting to the faces
        cc_hot = cvofa[ff_hot]
        cc_cold = cvofa[ff_cold]
        ccc_hot = cc_hot[:,0]
        ccc_cold = cc_cold[:,0]
    
    # input the specific values into the boundary (nodes)
        node_t[np.unique(nn_hot)] = 500.
        node_t[np.unique(nn_cold)] = 300.
    
        Tn_initial = 0. *np.ones(len(xy_no))
        Tn_initial[np.unique(nn_hot)] = 500
        Tn_initial[np.unique(nn_cold)] = 300
    
    # Calculate arithmetic average of the surrounding nodal values
    # Calculate arithmetic average of the surround cell values
    # Do iteration until it converges
    # Set the number of interation
        iter = 400
        for x in range(0,iter):
            for i in range(0, len(xy_cv)):
                cv_t[i] = np.mean(node_t[noofa[faocv[i]]])
    
            for j in range(0, len(xy_no)):
                if not -1 in cvofa[faono[j]]:
                    node_t[j] = np.mean(cv_t[np.unique(cvofa[[faono[j]]])])

        t = time.time() - tt

    # contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = node_t # values of data
    # define regular grid spatially covering input data
        n = 50
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
    # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
             np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
    # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)

        figure_folder = "../report/"
        figure_name = 'P2_TEMP FIELD_iter=400_' + filename.split('.')[0]+'.pdf'  # setting up file name

        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30

    # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('TEMPERATURE CONTOUR')

        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

###############################################################################
# "Linear operator problem"
###############################################################################

        ncv = len(cv_t) # length of cells
        nno = len(node_t)  # length of nodes
        An2cv = csr_matrix((ncv,nno)) # An2cv matrix
        Acv2n_int = csr_matrix((nno,ncv)) # Acv2n_int matrix
        Tn = Tn_initial

        tt_ln = time.time()
    
# input the weight value (= 1 / the number of nodes or cells)
# into the element connecting to nodes or cells surrounding a cell or a node

        for i in range(0, ncv):
            s_no = np.unique(noofa[faocv[i]]) # specific node number surrounding cell
            for ii in range(0, len(s_no)):
                An2cv[i,s_no[ii]] = 1. / len(noofa[faocv[i]])

        for j in range(0, nno):
            if not -1 in cvofa[faono[j]]:
                s_cv = cvofa[faono[j]]
                for jj in range(0, len(s_cv)):
                    Acv2n_int[j, s_cv[jj]] = 1. / len(np.unique(cvofa[[faono[j]]]))

        iter2 = iter
        for x in range(0,iter2):
            Tcv = An2cv.dot(Tn)
            Tn_int = Acv2n_int.dot(Tcv)
            Tn = Tn_int + Tn_initial

        t_ln = time.time() - tt_ln

# contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = Tn # values of data
# define regular grid spatially covering input data
        n = 50
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
# interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
             np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
# mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)

        figure_folder = "../report/"
        figure_name = 'P2_TEMP FIELD BY LINEAR OPERATOR_iter=400_' + filename.split('.')[0]+'.pdf'

        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30

# plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('TEMP FIELD CALCULATED BY LINEAR OPERATORS')

        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

# graph
        fig2 = figure(figsize=(14,6))
        ax1 = fig2.add_subplot(121)
        ax2 = fig2.add_subplot(122)
        ax1.set_title('LINEAR OPERATORS')

        ax1.spy(An2cv, precision=0.1, markersize=2)
        ax2.spy(Acv2n_int, precision=0.1, markersize=2)

        figure_folder = "../report/"
        figure_name = 'P2_OPERATOR GRAPH_' + filename.split('.')[0]+'.pdf' # change file name

        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

##################################################################
# Problem 3
##################################################################

if Problem3 :
    
    # Question 1
    
    icemcfd_project_folder = './'
    file_vec = ['Mesh_A_ncv_100.msh','Mesh_B_ncv_500.msh','Mesh_C_ncv_1500.msh','Mesh_D_ncv_10000.msh']
    rms = np.zeros(len(file_vec))
    jjjj = 0

    for filename in file_vec:
    
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(filename,node_reordering=True)

        xx = xy_no[:,0]
        yy = xy_no[:,1]
        lx =len(xx)
        ly =len(yy)
        Un = np.zeros(lx)
        Vn = np.zeros(lx)

        for i in range(0,len(xx)):
            Un[i] = np.cos(math.pi*xx[i])*np.sin(math.pi*yy[i]) #(xx[i]**2)*yy[i]
            Vn[i] = -np.sin(math.pi*xx[i])*np.cos(math.pi*yy[i]) #-(xx[i]*yy[i]**2)
    
        plt.title('Arrows scale with plot width, not view')
        plt.quiver(xx, yy, Un, Vn, units='width')
        figure_folder = "../report/"
        figure_name = 'P3_QUIVER PLOT_' + filename.split('.')[0]+'.pdf' # change file name
    
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

    # Question 2
        face_center = np.zeros((len(xy_fa),2))
        sum = np.zeros(len(xy_cv))
        divergence = np.zeros(len(xy_cv))
        area_store = np.zeros(len(xy_cv))


    # Un, Vn will be saved in this 2D array
    # normal vector to all faces
        for iii in range(0, len(xy_cv)): # example for cv=128 or 94
            faces = faocv[iii]
            vertices = xy_no[np.unique(noofa[faocv[iii]])]
            dot_product_x_dist = np.zeros(len(xy_fa))

            cv = xy_cv[iii]

            for iiii in faces:
                ff = len(faces)
                face_center[iiii,0] = np.mean(Un[noofa[iiii]]) # Un, x position
                face_center[iiii,1] = np.mean(Vn[noofa[iiii]]) # Vn, y position
            #matrix_normal_vector = np.zeros((len(faces), 2))
            
            # find normal vector
                nodes = noofa[iiii]
                vector_face = xy_no[nodes[0]] - xy_no[nodes[1]]   # vector parallel to face
                vector_normal = np.array([-vector_face[1], vector_face[0]])     # vector perpendicular to face
                norm = np.linalg.norm(vector_normal)
                normalized_unit_vector = vector_normal / norm

                vv = cv - xy_fa[iiii]   # vector from face center to cell centroid
                mm = np.inner(normalized_unit_vector, vv)

                if mm < 0:
                    final_normal_vector = normalized_unit_vector
                else:
                    final_normal_vector = -1.*normalized_unit_vector

                dot_product = np.inner(face_center[iiii], final_normal_vector)

                dot_1 = xy_no[nodes[0]]
                dot_2 = xy_no[nodes[1]]
                dist = math.hypot( dot_2[0] - dot_1[0], dot_2[1] - dot_1[1])
                dot_product_x_dist[iiii] = dist*dot_product
                sum = np.sum(dot_product_x_dist) # summation over all faces

    # calculate centroid of the polygon to sort vertices in counter-clockwise direction
                nv = len(vertices) # of corners
                x_no = vertices[:,0]
                y_no = vertices[:,1]
                cx = float(np.sum(x_no for x_no, y_no in vertices)) / nv
                cy = float(np.sum(y_no for x_no, y_no in vertices)) / nv
    # create a new list of vertices which includes angles
                vertices_ang = []
                for x_no, y_no in vertices:
                    dx = x_no - cx
                    dy = y_no - cy
                    angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                    vertices_ang.append((x_no, y_no, angle))
    # sort it using the angles
                vertices_ang.sort(key =lambda tup: tup[2])

    # Area by shoelace formula
                area = 0.0
                for k in range(nv):
                    j = (k + 1) % nv
                    area += vertices_ang[k][0]*vertices_ang[j][1]
                    area -= vertices_ang[j][0]*vertices_ang[k][1]
                area = abs(area) / 2.0
                
                area_store[iii] = area

                divergence[iii] = sum / area
                

    # contour
        x = xy_cv[:,0] # x coordinates of data
        y = xy_cv[:,1] # y coordinates of data
        phi = divergence # values of data
    # define regular grid spatially covering input data
        n = 200
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
    # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
                 # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
                 
        figure_folder = "../report/"
        figure_name = 'P3_DIV CONTOUR_' + filename.split('.')[0]+'.pdf'
                 
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
                 
    # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('DIVERGNECE CONTOUR')
                 
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()
        
        rms[jjjj] = np.sqrt(np.mean((area_store**2.)*(divergence**2.)))
        jjjj += 1

###############################################################################
# "Linear operator problem"
#  Two sparse linear operators, Dx_n2cv // Dy_n2cv
###############################################################################
        
        ncv = len(xy_cv) # length of cells
        nno = len(xy_no)  # length of nodes
        Dx_n2cv = csr_matrix((ncv,nno)) # Dx or Dy matrix
        Dy_n2cv = csr_matrix((ncv,nno)) # Dx or Dy matrix

        for icv in range(ncv) : # for example icv 128
            nodes_f = np.unique(noofa[faocv[icv]])
            face_f = faocv[icv]
            dist = np.zeros(len(xy_fa))
            final_normal_v = np.zeros((len(xy_fa),2))
            coc = xy_cv[icv] # center of cell

            nodes_xy = xy_no[np.unique(noofa[faocv[icv]])]

# calculate centroid of the polygon to sort vertices in counter-clockwise direction
            nv = len(nodes_xy) # of corners
            x_no = nodes_xy[:,0]
            y_no = nodes_xy[:,1]
            cx = float(np.sum(x_no for x_no, y_no in nodes_xy)) / nv
            cy = float(np.sum(y_no for x_no, y_no in nodes_xy)) / nv
    # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in nodes_xy:
                dx = x_no - cx
                dy = y_no - cy
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
        # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
        # Area by shoelace formula
            area = 0.0
            for k in range(nv):
                m = (k + 1) % nv
                area += nodes_ang[k][0]*nodes_ang[m][1]
                area -= nodes_ang[m][0]*nodes_ang[k][1]
            area = abs(area) / 2.0 # area of cell[icv]

            for jj in nodes_f: # for example, node[0]
                twofaces_onenode = faono[jj]
                common_faces = np.intersect1d(face_f, twofaces_onenode)
                ln_x = np.zeros(len(xy_fa))
                ln_y = np.zeros(len(xy_fa))


                for j in common_faces:
                    fff = len(common_faces)
                    two_nodes = noofa[j]

                    dot_11 = xy_no[two_nodes[0]]
                    dot_22 = xy_no[two_nodes[1]]
                    dist[j] = math.hypot( dot_22[0] - dot_11[0], dot_22[1] - dot_11[1])

        # find normal vector
                    vector_face = xy_no[two_nodes[0]] - xy_no[two_nodes[1]]   # vector parallel to face
                    vector_nn = np.array([-vector_face[1], vector_face[0]]) # vector perpendicular to face
                    norm = np.linalg.norm(vector_nn)
                    normalized_unit_normal_v = vector_nn / norm
            
                    vv = coc - xy_fa[j]   # vector from face center to cell centroid
                    mm = np.inner(normalized_unit_normal_v, vv)
            
                    if mm < 0:
                        final_normal_v[j] = normalized_unit_normal_v
                    else:
                        final_normal_v[j] = -1.*normalized_unit_normal_v
                    ln_x[j] = dist[j]*final_normal_v[j,0]  # x component of normal vector
                    ln_y[j] = dist[j]*final_normal_v[j,1]  # x component of normal vector
                Dx_n2cv[icv, jj] = np.sum(ln_x) / area
                Dy_n2cv[icv, jj] = np.sum(ln_y) / area

        Div = np.zeros(len(xy_cv))
        Div = Dx_n2cv.dot(Un) + Dy_n2cv.dot(Vn)
        
    # graph
        fig3 = figure(figsize=(14,6))
        ax1 = fig3.add_subplot(121)
        ax2 = fig3.add_subplot(122)
        ax1.set_title('DIV, X COMPONENT OPERATORS')
        ax2.set_title('DIV, Y COMPONENT OPERATORS')
    
        ax1.spy(Dx_n2cv, precision=0.1, markersize=2)
        ax2.spy(Dy_n2cv, precision=0.1, markersize=2)
                 
        figure_folder = "../report/"
        figure_name = 'P3_DIV OPERATOR GRAPH_' + filename.split('.')[0]+'.pdf'  # change file name
                 
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

# log-log plot
    Div_rms_vec = [rms[0], rms[1], rms[2], rms[3]]
    ncv_vec = [158., 448., 1568., 10063.]
    order_x_values = [1, 10**5]
    order1_y = [10, 10**-4]
    order2_y = [10, 10**-8]
    figure_folder = "../report/"
    figure_name = "P3_DIV RMS GRAPH.pdf"
        
    figwidth       = 16
    figheight      = 16
    lineWidth      = 3
    textFontSize   = 20
    gcafontSize    = 18
        
    fig = plt.figure(0, figsize = (figwidth, figheight))
    ax_left = fig.add_subplot(1,1,1)
    ax = ax_left
    plt.axes(ax)
        
    ax.loglog(ncv_vec,Div_rms_vec,'-r',linewidth=lineWidth)
    ax.loglog(order_x_values,order1_y,'-k',linewidth=lineWidth,ls='dashed')
    ax.loglog(order_x_values,order2_y,'-b',linewidth=lineWidth,ls='dashed')
    plt.setp(ax.get_xticklabels(),fontsize=gcafontSize)
    plt.setp(ax.get_yticklabels(),fontsize=gcafontSize)

    ax.grid('on',which='both')
    ax.set_ylim([10**-8,10**1])
    ax.set_xlim([10**2,3.*(10**4)])
    ax.set_xlabel(r"Number of Control Volumes (NCV)", fontsize=1.2*textFontSize)
    ax.set_ylabel(r"$\epsilon$",fontsize=1.3*textFontSize, rotation=90)
    ax.set_title(r"RMS ERROR OF DIV VS NCV",fontsize=1.3*textFontSize)
    
    black_line = mlines.Line2D([],[], color='black', label='First Order Accuracy',ls='dashed')
    red_line = mlines.Line2D([],[], color='red', label='Numeric Results')
    blue_line = mlines.Line2D([],[], color='blue', label='Second Order Accuracy',ls='dashed')
    ax.legend(handles=[red_line,black_line,blue_line])
    
    #Creates and saves figures
    figure_file_path = figure_folder + figure_name
    print "Saving figure: " + figure_file_path
    plt.tight_layout()
    plt.savefig(figure_file_path)
    plt.close()



keyboard()
