import os
import sys
import numpy as np
import scipy as sp
import scipy.sparse.linalg as splinalg
from scipy.interpolate import griddata
from scipy.sparse import csr_matrix
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
from pdb import set_trace as keyboard
import matplotlib.pyplot as plt
import matplotlib as mplt
import binascii
import numpy as N
import umesh_reader
from collections import Counter
from matplotlib.pyplot import figure, show
import numpy
from numpy import ma
import math
from sympy import Symbol, solve
import time
import matplotlib.lines as mlines
import scipy.sparse as scysparse
import bivariate_fit as fit
from numpy.linalg import inv
import scipy.ndimage
from scipy.linalg import solve
import matplotlib as mpl


##################################################################
#                                                                #
#   Jeongmin's Homework 4                                        #
#   Thanks to Prof. Scalo for all your advices for everything    #
#                                                                #
##################################################################

Problem1 = True
Problem1_1 = False
Problem1_2 = False
Problem1_3 = True

if Problem1 :
    icemcfd_project_folder = './'
    file_vec = ['tri_cavity_ncv=228.msh'] # 'tri_cavity_ncv=918.msh' ' tri_cavity_ncv=2033.msh'

    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)

        face_wall = np.squeeze(np.array(np.where(np.array(partofa) == 'WALL')))
        face_lid = np.squeeze(np.array(np.where(np.array(partofa) == 'LID')))
        node_wall = np.unique(noofa[face_wall])
        node_lid = np.unique(noofa[face_lid])
        node_duplicate = np.intersect1d(node_wall,node_lid)
        node_lid = np.setdiff1d(node_lid, node_duplicate)
        
        nno = len(xy_no)
        ncv = len(xy_cv)
        nfa = len(xy_fa)
        nno_int = nno-len(node_wall)-len(node_lid)
        nfa_int = nfa-len(face_wall)-len(face_lid)

        node_internal = np.linspace(0,nno_int-1,nno_int)
        
###############################################################################
#   Boundary conditions, viscosity, time-step size                            #
###############################################################################
        
        u_lid = np.array([1.0, 0.0])
        u_wall = np.array([0.0, 0.0])
        u_n = np.zeros((nno_int,2))
        nu = 0.1 # viscosity
        
#########################################################################
#   CFL                                                                 #
#########################################################################

        dt_conv = np.zeros(ncv)     # time step size for convection
        dt_diff = np.zeros(ncv)
        c_norm = np.zeros(ncv)

        area = np.zeros(ncv)
        for icv in range(ncv):
            cv_nodes = np.unique(noofa[faocv[icv]])     # nodes of the cell
            cv_faces = faocv[icv]    # faces of the cell
            cv_center = xy_cv[icv]    # center of the cell
            
            cv_nodes_xy = xy_no[cv_nodes]      # location of the nodes of the cell
            
            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            ino = len(cv_nodes_xy) # the number of nodes of the cell
            x_no = cv_nodes_xy[:,0]
            y_no = cv_nodes_xy[:,1]
            x_center = float(np.sum(x_no for x_no, y_no in cv_nodes_xy)) / ino
            y_center = float(np.sum(y_no for x_no, y_no in cv_nodes_xy)) / ino
            
            # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in cv_nodes_xy:
                dx = x_no - x_center
                dy = y_no - y_center
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
            # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
            # area by shoelace formula
            a = 0.0
            for k in range(ino):
                m = (k + 1) % ino
                a += nodes_ang[k][0]*nodes_ang[m][1]
                a -= nodes_ang[m][0]*nodes_ang[k][1]
            area[icv] = abs(a) / 2.0 # area of cell[icv]
        
        c_norm[icv] = 1.0
#        dt_conv[icv] = 0.85*math.sqrt(area[icv])/c_norm[icv]
#        min_dt_conv = min(dt_conv)      # minimum value of dt_conv
        final_alpha = nu
        
        CFL = np.zeros(ncv)
        max_CFL = 1
        while (max_CFL > 0.85):
            dt = input('Enter time step size:')
            for icv in range(ncv):
                CFL_diff = 4.0*final_alpha*dt/area[icv]
                CFL_conv = c_norm[icv]*dt/math.sqrt(area[icv])
                if CFL_conv >= CFL_diff:
                    CFL[icv] = CFL_conv
                else:
                    CFL[icv] = CFL_diff
            max_CFL = max(CFL)
            print(max_CFL)
#       keyboard()



##############################################################################
#   Bivariate polynomial                                                     #
##############################################################################

        B = csr_matrix((nno_int,nno_int))
        q_bc = np.zeros((nno_int,2))

        for i_node in range(nno_int):
            if not -1 in cvofa[faono[i_node]]:
                node_bivariate = np.unique(noofa[faono[i_node]])
                nno_bivariate = len(node_bivariate)
                mesh_abs_tol=0.0
                centroid = np.array(xy_no[i_node])
                xc = centroid[0]
                yc = centroid[1]

                xnyn = np.array(xy_no[node_bivariate])
                xn = xnyn[:,0]
                yn = xnyn[:,1]

                weights_x = np.zeros(nno_bivariate)
                weights_y = np.zeros(nno_bivariate)
                weights_dx = np.zeros(nno_bivariate)
                weights_dy = np.zeros(nno_bivariate)
                weights_dx2 = np.zeros(nno_bivariate)
                weights_dy2 = np.zeros(nno_bivariate)

                for ii_node in range(nno_bivariate):
                    phi_base = np.zeros(nno_bivariate)
                    phi_base[ii_node] = 1.0
                    weights_x[ii_node],weights_dx[ii_node],weights_dx2[ii_node] = fit.BiVarPolyFit_X(xc,yc,xn,yn,phi_base)
                    weights_y[ii_node],weights_dy[ii_node],weights_dy2[ii_node] = fit.BiVarPolyFit_Y(xc,yc,xn,yn,phi_base)
                    weights_sum_dx2dy2 = np.array(weights_dx2 + weights_dy2)
                        #if node_bivariate[ii_node] > nno_int:
                        #keyboard()
                    if node_bivariate[ii_node] in node_wall:
                        q_bc[i_node] = u_wall
                    elif node_bivariate[ii_node] in node_lid:
                        q_bc[i_node] += weights_sum_dx2dy2[ii_node]*u_lid
                    else:
                        B[i_node, node_bivariate[ii_node]] = weights_sum_dx2dy2[ii_node]

###############################################################################
#   U star (only for internal nodes)                                          #
###############################################################################

        iteration = input('Enter the number of iteration:')
        for n in range(iteration):
            u_star = (np.identity(nno_int) + dt*nu*B).dot(u_n)+dt*nu*q_bc
            #keyboard()

###############################################################################
#   Contour plot for u_star                                                   #
###############################################################################

            u_total_star = np.zeros((nno,2))
            for i in range(0,nno):
                if i in node_wall:
                    pass
                elif i in node_lid:
                    u_total_star[i] = np.array([1.0, 0.0])
                else:
                    #keyboard()
                    u_total_star[i] = u_star[i]
            # keyboard()

            u_magnitude_star = np.zeros(nno)
            for i in range(nno):
                u_magnitude_star[i] = np.linalg.norm(u_total_star[i])
            # keyboard()



##############################################################################
#   Interpolation of u_star on face centers                                  #
##############################################################################
            u_f_int = np.zeros((nfa_int,2))
            for i in range(0,nfa):
                #print i
                if i in face_wall:
                    #keyboard()
                    pass
                elif i in face_lid:
                    pass
                else:
                    u_f_int[i] = np.array([np.mean(u_total_star[noofa[i],0]),np.mean(u_total_star[noofa[i],1])])

#keyboard()

##############################################################################
#   Divergence operator                                                      #
##############################################################################

            Dx_nfa = csr_matrix((ncv,nfa)) #temporary matrix for saving all weights
            Dy_nfa = csr_matrix((ncv,nfa))

            Dx_f2cv = csr_matrix((ncv,nfa_int)) # Divergence X matrix
            Dy_f2cv = csr_matrix((ncv,nfa_int)) # Divergence Y matrix
        
            Div_u_star = csr_matrix((ncv,1))

            for icv in range(ncv):
                node_div = np.unique(noofa[faocv[icv]])
                face_div = faocv[icv]
                dist = np.zeros(nfa)
                final_normal_v = np.zeros((nfa,2))
                cocv = xy_cv[icv] # x,y location of control volume

                # calculate centroid of the polygon to sort vertices in counter-clockwise direction
                vertices = xy_no[node_div]
                nve = len(vertices)
                x_ver = vertices[:0]
                y_ver = vertices[:,1]
                x_center = float(np.sum(x_ver for x_ver, y_ver in vertices)) / nve
                y_center = float(np.sum(y_ver for x_ver, y_ver in vertices)) / nve

                # create a new list of vertices which includes angles and sort them based on angle information
                node_ang = []
                for x_ver, y_ver in vertices:
                    dx = x_ver - x_center
                    dy = y_ver - y_center
                    angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                    node_ang.append((x_ver, y_ver, angle))
                node_ang.sort(key =lambda tup: tup[2])

                # Area by shoelace formula
                area = 0.0
                for k in range(nve):
                    m = (k+1) % nve
                    area += node_ang[k][0]*node_ang[m][1]
                    area -= node_ang[m][0]*node_ang[k][1]
                area = abs(area) / 2.0

                for jj in node_div:
                    twofaces_onenode = faono[jj]
                    common_faces = np.intersect1d(face_div, twofaces_onenode)

                    ln_x = np.zeros(nfa)
                    ln_y = np.zeros(nfa)

                    # distance between two nodes
                    for j in common_faces:
                        ncf = len(common_faces)
                        twonodes = noofa[j]

                        dot_11 = xy_no[twonodes[0]]
                        dot_22 = xy_no[twonodes[1]]
                        dist[j] = math.hypot( dot_22[0] - dot_11[0], dot_22[1] - dot_11[1])

                        # find normal vector
                        vector_parallel = xy_no[twonodes[0]] - xy_no[twonodes[1]]   # vector parallel to face
                        vector_normal = np.array([-vector_parallel[1], vector_parallel[0]]) # vector perpendicular to face
                        norm = np.linalg.norm(vector_normal)
                        normalized_unit_normal_v = vector_normal / norm
    
                        vv = cocv - xy_fa[j]   # vector from face center to cell centroid
                        mm = np.inner(normalized_unit_normal_v, vv)
    
                        if mm < 0:
                            final_normal_v[j] = normalized_unit_normal_v
                        else:
                            final_normal_v[j] = -1.*normalized_unit_normal_v
                    
                        ln_x[j] = dist[j]*final_normal_v[j,0]  # x component of normal vector
                        ln_y[j] = dist[j]*final_normal_v[j,1]  # x component of normal vector
                        Dx_nfa[icv, j] = ln_x[j] / area
                        Dy_nfa[icv, j] = ln_y[j] / area

            for t in range(0, nfa_int):
                Dx_f2cv[:,t] = Dx_nfa[:,t]
                Dy_f2cv[:,t] = Dy_nfa[:,t]
#keyboard()

            q_bc_div = np.zeros(ncv)
            for icv in range(0, ncv):
                for tt in face_wall:
                    q_bc_div[icv] += Dx_nfa[icv,tt]*0.0 + Dy_nfa[icv,tt]*0.0
                for ttt in face_lid:
                    q_bc_div[icv] += Dx_nfa[icv,ttt]*1.0 + Dy_nfa[icv,ttt]*0.0

            Div_u_star = Dx_f2cv.dot(u_f_int[:,0]) + Dy_f2cv.dot(u_f_int[:,1]) + q_bc_div

##############################################################################
#   Laplacian operator                                                       #
##############################################################################

##############################################################################
#   Gradient operator                                                        #
##############################################################################

            temporary_Gx = csr_matrix((ncv,ncv))
            temporary_Gy = csr_matrix((ncv,ncv))

            for icv in range(ncv):
                # neighboring CVs including centroid
                scv = np.unique(cvofa[faocv[icv]])
                # remove ghost cells
                scv = np.array([num for num in scv if num >=0])
                index = np.where(scv == icv)
                sscv = np.delete(scv, index) # only neighboring CVs
            
                # stencil at corners
                if len(sscv) == 1:
                    stencil = np.unique(cvofa[faocv[sscv[0]]])
                    index_2 = np.where( stencil == icv)
                    sscv = np.delete(stencil, index_2)
        
                a_icv = 0.0
                b_icv = 0.0
                c_icv = 0.0
            
                dx = np.zeros(ncv)
                dy = np.zeros(ncv)
                ww = np.zeros(ncv)
            
            
                k = 0
                for xx in sscv:
                    dx[xx] = xy_cv[xx,0] - xy_cv[icv,0]
                    dy[xx] = xy_cv[xx,1] - xy_cv[icv,1]
                    ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
            
            
                for xxx in sscv:
                    a_icv += ((ww[xxx])**2.)*(dx[xxx])**2.
                    b_icv += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                    c_icv += ((ww[xxx])**2.)*(dy[xxx])**2.
                D = a_icv*c_icv-(b_icv)**2.
                if D == 0:
                    keyboard()
            
                for kk in sscv:
                    temporary_Gx[icv,icv] += -1.*c_icv/D*((ww[kk])**2.)*dx[kk] + b_icv/D*((ww[kk])**2.)*dy[kk]
                
                    temporary_Gx[icv,kk] = c_icv/D*((ww[kk])**2.)*dx[kk] - b_icv/D*((ww[kk])**2.)*dy[kk]
                
                    temporary_Gy[icv,icv] += b_icv/D*((ww[kk])**2.)*dx[kk] -1.*a_icv/D*((ww[kk])**2.)*dy[kk]
                
                    temporary_Gy[icv,kk] = -1.*b_icv/D*((ww[kk])**2.)*dx[kk] + a_icv/D*((ww[kk])**2.)*dy[kk]

##############################################################################
#   Interpolation for gradient operator (ncv,ncv) --> (nfa,ncv)              #
##############################################################################

            xy_fa_int = np.zeros((nfa_int,2))
            for uu in range(0, nfa_int):
                xy_fa_int[uu] = xy_fa[uu]
                itpl = csr_matrix((nfa_int, ncv))
            
            for mm in range(0, len(xy_fa)):
                if mm in face_lid:
                    pass # print "cold boundary face"
                elif mm in face_wall:
                    pass # print "hot bounadry face"
                else:
                    two_cvs = cvofa[mm]
                    cv1 = two_cvs[0]
                    cv2 = two_cvs[1]
                                
                    l_1 = math.hypot(xy_cv[cv1][0] - xy_fa[mm][0], xy_cv[cv1][1] - xy_fa[mm][1])
                    l_2 = math.hypot(xy_cv[cv2][0] - xy_fa[mm][0], xy_cv[cv2][1] - xy_fa[mm][1])
                                
                    # interpolation weights
                    w1 = l_2/(l_1 + l_2)
                    w2 = l_1/(l_1 + l_2)
                                
                    itpl[mm, cv1] = w1
                    itpl[mm, cv2] = w2

            Gx = itpl.dot(temporary_Gx)
            Gy = itpl.dot(temporary_Gy)

##############################################################################
#   Matrix A = Div dot Gradient                                              #
##############################################################################

            A = Dx_f2cv.dot(Gx) + Dy_f2cv.dot(Gy)
            AA = A.todense()
            rank = np.linalg.matrix_rank(AA)

##############################################################################
#   Pressure, P_n+1, Poisson equation                                        #
##############################################################################

            pressure = sp.sparse.linalg.spsolve(A, Div_u_star/dt)

##############################################################################
#   U_n+1 based on U_star and Pressure                                       #
##############################################################################
##############################################################################
#   1. Estimate the velocity at the nodal location by arithmetic average     #
##############################################################################
            if Problem1_1:
                u_nn_face = np.zeros((nfa_int,2))
                u_nn_face[:,0] = u_f_int[:,0] - dt*Gx*pressure
                u_nn_face[:,1] = u_f_int[:,1] - dt*Gy*pressure

                x_fa_int = np.zeros(nfa_int) # the position of internal faces
                y_fa_int = np.zeros(nfa_int)
                for yyy in range(0, nfa_int):
                    x_fa_int[yyy] = xy_fa[yyy,0]
                    y_fa_int[yyy] = xy_fa[yyy,1]
        
##############################################################################
#   Re-calculation from face to nodal locations                              #
##############################################################################

                u_nn_node = np.zeros((nno_int,2))
#keyboard()
                for ino in range(0,nno_int):
                    u_nn_node[ino,0] = np.mean(u_nn_face[faono[ino],0])
                    u_nn_node[ino,1] = np.mean(u_nn_face[faono[ino],1])
        
##############################################################################
#   Update u_n <--u_nn_node                                                  #
##############################################################################
        #keyboard()
                u_n = u_nn_node

##############################################################################
#   Using [nno_int x ncv] shaped gradient operator,
##############################################################################
##############################################################################
#   Gradient operator                                                        #
##############################################################################
            if Problem1_2:

                temporary_Gx_2 = csr_matrix((ncv,ncv))
                temporary_Gy_2 = csr_matrix((ncv,ncv))
    
                for icv in range(ncv):
                    # neighboring CVs including centroid
                    scv = np.unique(cvofa[faocv[icv]])
                    # remove ghost cells
                    scv = np.array([num for num in scv if num >=0])
                    index = np.where(scv == icv)
                    sscv = np.delete(scv, index) # only neighboring CVs
            
                    # stencil at corners
                    if len(sscv) == 1:
                        stencil = np.unique(cvofa[faocv[sscv[0]]])
                        index_2 = np.where( stencil == icv)
                        sscv = np.delete(stencil, index_2)
    
                    a_icv = 0.0
                    b_icv = 0.0
                    c_icv = 0.0
            
                    dx = np.zeros(ncv)
                    dy = np.zeros(ncv)
                    ww = np.zeros(ncv)
            
            
                    k = 0
                    for xx in sscv:
                        dx[xx] = xy_cv[xx,0] - xy_cv[icv,0]
                        dy[xx] = xy_cv[xx,1] - xy_cv[icv,1]
                        ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
        
        
                    for xxx in sscv:
                        a_icv += ((ww[xxx])**2.)*(dx[xxx])**2.
                        b_icv += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                        c_icv += ((ww[xxx])**2.)*(dy[xxx])**2.
                    D = a_icv*c_icv-(b_icv)**2.
                    if D == 0:
                        keyboard()
                    for kk in sscv:
                        temporary_Gx_2[icv,icv] += -1.*c_icv/D*((ww[kk])**2.)*dx[kk] + b_icv/D*((ww[kk])**2.)*dy[kk]
                    
                        temporary_Gx_2[icv,kk] = c_icv/D*((ww[kk])**2.)*dx[kk] - b_icv/D*((ww[kk])**2.)*dy[kk]
                
                        temporary_Gy_2[icv,icv] += b_icv/D*((ww[kk])**2.)*dx[kk] -1.*a_icv/D*((ww[kk])**2.)*dy[kk]
                
                        temporary_Gy_2[icv,kk] = -1.*b_icv/D*((ww[kk])**2.)*dx[kk] + a_icv/D*((ww[kk])**2.)*dy[kk]

##############################################################################
#   Interpolation for gradient operator (ncv,ncv) --> (nno_int,ncv)          #
##############################################################################

                xy_no_int = np.zeros((nno_int,2))
                for uu in range(0, nno_int):
                    xy_no_int[uu] = xy_no[uu]
                    new_itpl = csr_matrix((nno_int, ncv))
        
                for mm in range(0, len(xy_no)):
                    if mm in node_lid:
                        pass # print "cold boundary face"
                    elif mm in node_wall:
                        pass # print "hot bounadry face"
                    else:
                        cvs = np.unique(cvofa[faono[mm]])
                        dist_2 = np.zeros(len(cvs))

                        for i in range(0, len(cvs)):
                            dist_2[i] = math.hypot(xy_cv[cvs[i]][0] - xy_no[mm][0], xy_cv[cvs[i]][1] - xy_no[mm][1])

                        # interpolation weights
                        for i in range(0, len(cvs)):
                            w = dist_2[i] / np.sum(dist_2)
                            new_itpl[mm,cvs[i]] = w

                new_Gx = new_itpl.dot(temporary_Gx_2)
                new_Gy = new_itpl.dot(temporary_Gy_2)

##############################################################################
#   Update U_nn
##############################################################################

# keyboard()
                u_nn_node = np.zeros((nno_int,2))
                u_nn_node[:,0] = np.squeeze(u_star[:,0]) - dt*new_Gx*pressure
                u_nn_node[:,1] = np.squeeze(u_star[:,1]) - dt*new_Gy*pressure

                u_n = u_nn_node

                x_no_int = np.zeros(nno_int) # the position of internal faces
                y_no_int = np.zeros(nno_int)
                for yyy in range(0, nno_int):
                    x_no_int[yyy] = xy_no[yyy,0]
                    y_no_int[yyy] = xy_no[yyy,1]

                x_fa_int = np.zeros(nfa_int) # the position of internal faces
                y_fa_int = np.zeros(nfa_int)
                for yyy in range(0, nfa_int):
                    x_fa_int[yyy] = xy_fa[yyy,0]
                    y_fa_int[yyy] = xy_fa[yyy,1]
        


##############################################################################
#   3. Evaluate the pressure gradient at face and estimate its value on      #
#      nodal location by taking an arithmetic average of it                  #
##############################################################################

            if Problem1_3:
            
                xy_no_int = np.zeros((nno_int,2))
                for uu in range(0, nno_int):
                    xy_no_int[uu] = xy_no[uu]
                    new_itpl_2 = csr_matrix((nno_int, nfa_int))
        
                for mm in range(0, len(xy_no)):
                    if mm in node_lid:
                        pass # print "cold boundary face"
                    elif mm in node_wall:
                        pass # print "hot bounadry face"
                    else:
                        fs = np.unique(faono[mm])
                        for i in range(0, len(fs)):
                            new_itpl_2[mm, fs[i]] = 1./len(fs)
                        
                        
                new_Gx_2 = new_itpl_2.dot(Gx)
                new_Gy_2 = new_itpl_2.dot(Gy)

##############################################################################
#   Update U_nn
##############################################################################

# keyboard()
                u_nn_node = np.zeros((nno_int,2))
                u_nn_node[:,0] = np.squeeze(u_star[:,0]) - dt*new_Gx_2*pressure
                u_nn_node[:,1] = np.squeeze(u_star[:,1]) - dt*new_Gy_2*pressure
    
                u_n = u_nn_node

                x_no_int = np.zeros(nno_int) # the position of internal faces
                y_no_int = np.zeros(nno_int)
                for yyy in range(0, nno_int):
                    x_no_int[yyy] = xy_no[yyy,0]
                    y_no_int[yyy] = xy_no[yyy,1]
            
                x_fa_int = np.zeros(nfa_int) # the position of internal faces
                y_fa_int = np.zeros(nfa_int)
                for yyy in range(0, nfa_int):
                    x_fa_int[yyy] = xy_fa[yyy,0]
                    y_fa_int[yyy] = xy_fa[yyy,1]
            

###################################################################################
#   Post-processing
###################################################################################


        if Problem1_1:

            plt.title('Arrows scale with plot width, not view')
            #plt.quiver(x_fa_int, y_fa_int, u_f_int[:,0], u_f_int[:,1], units='width')
            plt.quiver(x_fa_int, y_fa_int, u_nn_face[:,0], u_nn_face[:,1], units='width', color='r')
            #plt.legend(['V_STAR','V_TILDE'], loc='upper left')
    
    
            figure_folder = "../report/"
            figure_name = 'P1_METHOD 1_QUIVER PLOT_' + filename.split('.')[0]+ '_iteration='+ str(iteration) + '.pdf' # change file name
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
        
        
            total_u_n = np.zeros((nno,2))
            for i in range(nno):
                if i in node_lid:
                    total_u_n[i] = np.array([1.0, 0.0])
                elif i in node_wall:
                    pass
                else:
                    total_u_n[i] = u_n[i]

            magnitude_u_n = np.zeros(nno)
            for i in range(nno):
                magnitude_u_n[i] = np.linalg.norm(total_u_n[i])

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = magnitude_u_n # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(np.isnan(Z),Z)
        
            figure_folder = "../report/"
            figure_name = 'P1_METHOD 1 FLOW FIELD CONTOUR_' + filename.split('.')[0]+ '_iteration='+ str(iteration)+'.jpg'  # setting up file name
            
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
        
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud')
            plt.colorbar()
            ax.set_title('METHOD 1 FLOW CONTOUR')
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

            # X and Y velocity plot
            ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                          np.vstack(total_u_n[:,1].flatten()),(X,Y),method='cubic').reshape(X.shape)
            plt.plot(X[320], ZZ[320])
            plt.axis([0,1,-1.,1.])
            plt.xlabel('X')
            plt.ylabel('Y-VELOCITY')
            plt.title('Y-VELOCITY ALONG THE HORIZONTAL LINE')

            figure_folder = "../report/"
            figure_name = 'P1_METHOD_1 Y-VELOCITY'+ filename.split('.')[0]+ '.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

            ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, \
              np.vstack(total_u_n[:,0].flatten()),(X,Y),method='cubic').reshape(X.shape)
            plt.plot(X[320], ZZ[320])
            plt.axis([0,1,-1.,1.])
            plt.xlabel('X')
            plt.ylabel('X-VELOCITY')
            plt.title('X-VELOCITY ALONG THE HORIZONTAL LINE')
    
            figure_folder = "../report/"
            figure_name = 'P1_METHOD_1 X-VELOCITY'+ filename.split('.')[0]+ '.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()


        if Problem1_2:

            plt.title('Arrows scale with plot width, not view')
            #plt.quiver(x_fa_int, y_fa_int, u_f_int[:,0], u_f_int[:,1], units='width')
            plt.quiver(x_no_int, y_no_int, u_nn_node[:,0], u_nn_node[:,1], units='width', color='r')
            #plt.legend(['V_STAR','V_TILDE'], loc='upper left')
    
    
            figure_folder = "../report/"
            figure_name = 'P1_METHOD 2_QUIVER PLOT_' + filename.split('.')[0]+ '_iteration='+ str(iteration)+'.pdf' # change file name
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
        
        
            total_u_n = np.zeros((nno,2))
            for i in range(nno):
                if i in node_lid:
                    total_u_n[i] = np.array([1.0, 0.0])
                elif i in node_wall:
                    pass
                else:
                    total_u_n[i] = u_n[i]

            magnitude_u_n = np.zeros(nno)
            for i in range(nno):
                magnitude_u_n[i] = np.linalg.norm(total_u_n[i])

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = magnitude_u_n # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(np.isnan(Z),Z)
        
            figure_folder = "../report/"
            figure_name = 'P1_METHOD 2 FLOW FIELD CONTOUR_' + filename.split('.')[0]+ '_iteration='+ str(iteration)+'.jpg'  # setting up file name
            
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
            
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud')
            plt.colorbar()
            ax.set_title('METHOD 2 FLOW CONTOUR')
            
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

            # X and Y velocity plot
            ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, \
              np.vstack(total_u_n[:,1].flatten()),(X,Y),method='cubic').reshape(X.shape)
            plt.plot(X[320], ZZ[320])
            plt.axis([0,1,-1.,1.])
            plt.xlabel('X')
            plt.ylabel('Y-VELOCITY')
            plt.title('Y-VELOCITY ALONG THE HORIZONTAL LINE')
    
            figure_folder = "../report/"
            figure_name = 'P1_METHOD_2 Y-VELOCITY'+ filename.split('.')[0]+ '.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
            
            ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                          np.vstack(total_u_n[:,0].flatten()),(X,Y),method='cubic').reshape(X.shape)
            plt.plot(X[320], ZZ[320])
            plt.axis([0,1,-1.,1.])
            plt.xlabel('X')
            plt.ylabel('X-VELOCITY')
            plt.title('X-VELOCITY ALONG THE HORIZONTAL LINE')
                          
            figure_folder = "../report/"
            figure_name = 'P1_METHOD_2 X-VELOCITY'+ filename.split('.')[0]+ '.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()


        if Problem1_3:

            plt.title('Arrows scale with plot width, not view')
            #plt.quiver(x_fa_int, y_fa_int, u_f_int[:,0], u_f_int[:,1], units='width')
            plt.quiver(x_no_int, y_no_int, u_nn_node[:,0], u_nn_node[:,1], units='width', color='r')
            #plt.legend(['V_STAR','V_TILDE'], loc='upper left')
            
            
            figure_folder = "../report/"
            figure_name = 'P1_METHOD 3_QUIVER PLOT_' + filename.split('.')[0]+ '_iteration='+ str(iteration)+'.pdf' # change file name
            
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()


            total_u_n = np.zeros((nno,2))
            for i in range(nno):
                if i in node_lid:
                    total_u_n[i] = np.array([1.0, 0.0])
                elif i in node_wall:
                    pass
                else:
                    total_u_n[i] = u_n[i]
    
            magnitude_u_n = np.zeros(nno)
            for i in range(nno):
                magnitude_u_n[i] = np.linalg.norm(total_u_n[i])

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = magnitude_u_n # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(np.isnan(Z),Z)
                         
            figure_folder = "../report/"
            figure_name = 'P1_METHOD 3 FLOW FIELD CONTOUR_' + filename.split('.')[0]+ '_iteration='+ str(iteration)+'.jpg'  # setting up file name
                         
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
                         
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud')
            plt.colorbar()
            ax.set_title('METHOD 3 FLOW CONTOUR')
                         
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

            # X and Y velocity plot
            ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, \
              np.vstack(total_u_n[:,1].flatten()),(X,Y),method='cubic').reshape(X.shape)
            plt.plot(X[320], ZZ[320])
            plt.axis([0,1,-1.,1.])
            plt.xlabel('X')
            plt.ylabel('Y-VELOCITY')
            plt.title('Y-VELOCITY ALONG THE HORIZONTAL LINE')
    
            figure_folder = "../report/"
            figure_name = 'P1_METHOD_3 Y-VELOCITY'+ filename.split('.')[0]+ '.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
            
            ZZ = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                          np.vstack(total_u_n[:,0].flatten()),(X,Y),method='cubic').reshape(X.shape)
            plt.plot(X[320], ZZ[320])
            plt.axis([0,1,-1.,1.])
            plt.xlabel('X')
            plt.ylabel('X-VELOCITY')
            plt.title('X-VELOCITY ALONG THE HORIZONTAL LINE')
                          
            figure_folder = "../report/"
            figure_name = 'P1_METHOD_3 X-VELOCITY'+ filename.split('.')[0]+ '.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

keyboard()
