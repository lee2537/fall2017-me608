import os
import sys
import numpy as np
import scipy as sp
import scipy.sparse.linalg as splinalg
from scipy.interpolate import griddata
from scipy.sparse import csr_matrix
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
from pdb import set_trace as keyboard
import matplotlib.pyplot as plt
import matplotlib as mplt
import binascii
import numpy as N
import umesh_reader
from collections import Counter
from matplotlib.pyplot import figure, show
import numpy
from numpy import ma
import math
from sympy import Symbol, solve
import time
import matplotlib.lines as mlines
import scipy.sparse as scysparse
import bivariate_fit as fit
from numpy.linalg import inv
import scipy.ndimage
from scipy.linalg import solve
import matplotlib as mpl


##################################################################
#                                                                #
#   Jeongmin's Homework 3                                        #
#   Thanks to Prof. Scalo for all your advices for everything    #
#                                                                #
##################################################################

Problem1 = True
Problem1_2 = False
Problem1_3 = True
Problem1_4 = False
Problem2 = False


if Problem1 :
    icemcfd_project_folder = './'
    file_vec = ['quad_v3.msh'] # mesh_c_quality_1.msh, hw2_mesh2.msh, New_mesh_v1.msh, quad_v3.msh

    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
        
        ### Problem1 - (1) ###
        nno = len(xy_no)
        ncv = len(xy_cv)
        dt_conv = np.zeros(ncv)     # time step size for convection
        dt_conv_2 = np.zeros(ncv)
        dt_diff = np.zeros(ncv)     # time step size for diffusion
        area = np.zeros(ncv)
        c_norm = np.zeros(ncv)
        
        for icv in range(ncv):
            cv_nodes = np.unique(noofa[faocv[icv]])     # nodes of the cell
            cv_faces = faocv[icv]    # faces of the cell
            cv_center = xy_cv[icv]    # center of the cell
            
            cv_nodes_xy = xy_no[cv_nodes]      # location of the nodes of the cell
            
            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            ino = len(cv_nodes_xy) # the number of nodes of the cell
            x_no = cv_nodes_xy[:,0]
            y_no = cv_nodes_xy[:,1]
            x_center = float(np.sum(x_no for x_no, y_no in cv_nodes_xy)) / ino
            y_center = float(np.sum(y_no for x_no, y_no in cv_nodes_xy)) / ino
            
            # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in cv_nodes_xy:
                dx = x_no - x_center
                dy = y_no - y_center
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
            # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
            # area by shoelace formula
            a = 0.0
            for k in range(ino):
                m = (k + 1) % ino
                a += nodes_ang[k][0]*nodes_ang[m][1]
                a -= nodes_ang[m][0]*nodes_ang[k][1]
            area[icv] = abs(a) / 2.0 # area of cell[icv]
            
            # non-trivial divergence free vector field and the norm of it
            Un = np.cos(math.pi*x_center)*np.sin(math.pi*y_center)
            Vn = -np.sin(math.pi*x_center)*np.cos(math.pi*y_center)
            c = np.array([Un, Vn])
            c_norm[icv] = np.linalg.norm(c)
            
            dt_conv[icv] = 0.85*math.sqrt(area[icv])/c_norm[icv]

        min_dt_conv = min(dt_conv)      # minimum value of dt_conv

        # deciding the alpha
        alpha = np.zeros(ncv)
        for icv in range(ncv):
            alpha[icv] = 0.85*area[icv]/4.0/min_dt_conv
            final_alpha = min(alpha)

        for icv in range(ncv):
            dt_diff[icv] = 0.85*area[icv]/4./final_alpha
            min_dt_diff = min(dt_diff)

        # calculating CFL and make a plot of CFL
        CFL = np.zeros(ncv)
        dt = input('Enter time step size:')
        for icv in range(ncv):
            CFL_diff = 4.0*final_alpha*dt/area[icv]
            CFL_conv = c_norm[icv]*dt/math.sqrt(area[icv])
            if CFL_conv >= CFL_diff:
                CFL[icv] = CFL_conv
            else:
                CFL[icv] = CFL_diff
        print(max(CFL))

        # contour
        x = xy_cv[:,0] # x coordinates of data
        y = xy_cv[:,1] # y coordinates of data
        phi = CFL # values of data
        # define regular grid spatially covering input data
        n = 400
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)

        # define parameters for region(s) to be masked, initializes mask array
        circ1_cent = [1.25,0.25]
        circ2_cent = [1.25,0.75]
        circ3_cent = [1.75,0.25]
        circ4_cent = [1.75,0.75]
        circ_rad = 0.07
    
        mask_mat = np.zeros(np.size(X)).reshape(X.shape)
    
        # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
        for ix,zx in enumerate(X[1]):
            for jy,zy in enumerate(Y[1]):
                dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)

                if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                    mask_mat[ix,jy]=np.nan

        # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)

        figure_folder = "../report/"
        figure_name = 'P1_CFL=0.85_' + filename.split('.')[0]+'.jpg'  # setting up file name
        
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
        
        # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=0.85, vmin=0.)
        plt.colorbar()
        ax.set_title('CFL CONTOUR')
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

        ### Problem1 - (2) ###
        ### RK2, purely diffusive problem ###

        faces_cold = np.where(np.array(partofa) == 'COLD')
        faces_hot = np.where(np.array(partofa) == 'HOT')
        nodes_cold = np.unique(noofa[faces_cold])
        nodes_hot = np.unique(noofa[faces_hot])
        
        nno_internal_nodes = nno - len(nodes_cold) - len(nodes_hot)
        A = csr_matrix((nno_internal_nodes,nno_internal_nodes))
        
        phi_exact = np.zeros(len(xy_no))
        d2phidx2_exact = np.zeros(len(xy_no))
        d2phidy2_exact = np.zeros(len(xy_no))
        phi_num_x = np.zeros(len(xy_no))
        phi_num_y = np.zeros(len(xy_no))
        dphidx_num = np.zeros(len(xy_no))
        dphidy_num = np.zeros(len(xy_no))
        d2phidx2_num = np.zeros(len(xy_no))
        d2phidy2_num = np.zeros(len(xy_no))
        
        # source terms setting
        b_source = np.zeros(nno_internal_nodes)
        b_bc = np.zeros(nno_internal_nodes)
        
        #for i in range(0, len(xy_no)):
        #    if ( 0.4 <= xy_no[i,0] <= 0.6):
        #        if (0.3 <= xy_no[i,1] <= 0.7):
        #            b_source[i] = 0.0

        for i_node in range(nno_internal_nodes):
            if not -1 in cvofa[faono[i_node]]:
                nodes_polyfit = np.unique(noofa[faono[i_node]])     # including the centroid itself
                index = np.where(nodes_polyfit == i_node)
                index_str = ''.join(str(e) for e in index)

                nno_polyfit = len(nodes_polyfit)
                mesh_abs_tol = 0.0
                centroid_location = np.array(xy_no[i_node])
                xc = centroid_location[0]
                yc = centroid_location[1]
                
                # location of nodes around a centroid and a center node
                xnyn = np.array(xy_no[nodes_polyfit])
                xn = xnyn[:,0]
                yn = xnyn[:,1]
                
                weights_x = np.zeros(nno_polyfit)
                weights_y = np.zeros(nno_polyfit)
                weights_dx = np.zeros(nno_polyfit)
                weights_dy = np.zeros(nno_polyfit)
                weights_dx2 = np.zeros(nno_polyfit)
                weights_dy2 = np.zeros(nno_polyfit)
                
                
                for ii in range(nno_polyfit):
                    phi_base = np.zeros(nno_polyfit)
                    phi_base[ii] = 1.0
                    weights_x[ii],weights_dx[ii],weights_dx2[ii] = fit.BiVarPolyFit_X(xc,yc,xn,yn,phi_base)
                    weights_y[ii],weights_dy[ii],weights_dy2[ii] = fit.BiVarPolyFit_Y(xc,yc,xn,yn,phi_base)
                    
                    weights_sum_dx2dy2 = -1.0*final_alpha*np.array(weights_dx2 + weights_dy2)
                    
                    if nodes_polyfit[ii] in nodes_cold:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ii]*200
                    elif nodes_polyfit[ii] in nodes_hot:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ii]*500
                    else:
                        A[i_node, nodes_polyfit[ii]] = weights_sum_dx2dy2[ii]
    
        q_bc = b_bc + b_source
        #dt =  min_dt_conv
        interation = input('Enter the number of iteration:')
        phi_solution = np.zeros((nno_internal_nodes,2))
        phi_solution[:,0] = 300.0*np.array(np.ones(nno_internal_nodes)) # initial values
            
        if Problem1_2:
            for n in range(interation):
                phi_1 = phi_solution[:,0] + dt*(-1.*A.dot(phi_solution[:,0]) + q_bc)
                R_1 = -1.*A.dot(phi_1) + q_bc
                R_n = -1.*A.dot(phi_solution[:,0]) + q_bc

                phi_solution[:,1] = phi_solution[:,0] + 1./2.*dt*(R_n + R_1)
                phi_solution[:,0] = phi_solution[:,1]
                
                # phi_solution[:,1] = dt*np.array(q_bc - A.dot(phi_solution[:,0])) + phi_solution[:,0]
                # phi_solution[:,0] = phi_solution[:,1]
        
            final_solution = np.zeros(len(xy_no))
            for nn in range(0, len(xy_no)):
                if nn in nodes_cold:
                    final_solution[nn] = 200.
                elif nn in nodes_hot:
                    final_solution[nn] = 500.
                else:
                    final_solution[nn] = phi_solution[nn,1]
        
            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = final_solution # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
                
            # define parameters for region(s) to be masked, initializes mask array
            circ1_cent = [1.25,0.25]
            circ2_cent = [1.25,0.75]
            circ3_cent = [1.75,0.25]
            circ4_cent = [1.75,0.75]
            circ_rad = 0.07
                         
            mask_mat = np.zeros(np.size(X)).reshape(X.shape)
                         
            # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
            for ix,zx in enumerate(X[1]):
                for jy,zy in enumerate(Y[1]):
                    dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                    dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                    dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                    dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)
                                 
                    if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                        mask_mat[ix,jy]=np.nan

            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)
    
            figure_folder = "../report/"
            figure_name = 'P1_TEMP CONTOUR DIFFUSION RK2 CFL=0.85 t=10s_' + filename.split('.')[0]+'.jpg'  # setting up file name
        
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
            
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=500, vmin=200.)
            plt.colorbar()
            ax.set_title('TEMP CONTOUR AT DIFFERENT STAGE, RK2 (ONLY DIFFUSION)')
            
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
        
        
        ### Problem1 - (3) ###
        ### RK2, purely convective problem ###
        if Problem1_3:
            Cx = np.zeros(nno)
            Cy = np.zeros(nno)
            Cx_int = np.zeros(nno_internal_nodes)
            Cy_int = np.zeros(nno_internal_nodes)
                           
           ### Generating "gradient operator" by using a least squre prodcedure ###
                           
            faces_cold = np.where(np.array(partofa) == 'COLD')
            faces_hot = np.where(np.array(partofa) == 'HOT')
            nodes_cold = np.unique(noofa[faces_cold])
            nodes_hot = np.unique(noofa[faces_hot])
                    
            nno_internal_nodes = nno - len(nodes_cold) - len(nodes_hot)
                    
            Gx = csr_matrix((nno_internal_nodes,nno))
            Gy = csr_matrix((nno_internal_nodes,nno))
            #Gx_int = csr_matrix((nno_internal_nodes,nno_internal_nodes))
            #Gy_int = csr_matrix((nno_internal_nodes,nno_internal_nodes))
            
            for nn in range(nno):
                Cx[nn] = np.cos(math.pi*xy_no[nn,0])*np.sin(math.pi*xy_no[nn,1])
                Cy[nn] = -np.sin(math.pi*xy_no[nn,0])*np.cos(math.pi*xy_no[nn,1])
            
            Cx_int = np.identity(nno_internal_nodes)*Cx[0:nno_internal_nodes] #np.identity(nno_internal_nodes)*Cx[0:nno_internal_nodes]
            Cy_int = np.identity(nno_internal_nodes)*Cy[0:nno_internal_nodes] #np.identity(nno_internal_nodes)*Cy[0:nno_internal_nodes]
        
            
            
            # numerical procedure
            for ino in range(nno_internal_nodes):
                sno = np.unique(noofa[faono[ino]]) # neighboring vertices including centroid
                esno = np.unique(noofa[np.unique(np.concatenate(faono[sno], axis=0))])

                index = np.where(esno == ino)
                only_sno = np.delete(esno, index) # only neighboring CVs
                
                for tt in only_sno:
                    vector_1 = xy_no[ino] - xy_no[tt]
                    inner_dot = vector_1[0]*Cx[ino]+vector_1[1]*Cy[ino]
                    if inner_dot >=0:
                        pass
                    else:
                        index2 = np.where(only_sno == tt)
                        only_sno = np.delete(only_sno, index2)
                
                a_ino = 0.0
                b_ino = 0.0
                c_ino = 0.0
                                        
                dx = np.zeros(nno)
                dy = np.zeros(nno)
                ww = np.zeros(nno)
                                        
                k = 0
                for xx in only_sno:
                    dx[xx] = xy_no[xx,0] - xy_no[ino,0]
                    dy[xx] = xy_no[xx,1] - xy_no[ino,1]
                    ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
                        
                for xxx in only_sno:
                    a_ino += ((ww[xxx])**2.)*(dx[xxx])**2.
                    b_ino += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                    c_ino += ((ww[xxx])**2.)*(dy[xxx])**2.
                D = a_ino*c_ino-(b_ino)**2.
                if D==0:
                    keyboard()

                for kk in only_sno:
                    Gx[ino,ino] += -1.*c_ino/D*((ww[kk])**2.)*dx[kk] + b_ino/D*((ww[kk])**2.)*dy[kk]
                                                        
                    Gx[ino,kk] = c_ino/D*((ww[kk])**2.)*dx[kk] - b_ino/D*((ww[kk])**2.)*dy[kk]
                                                        
                    Gy[ino,ino] += b_ino/D*((ww[kk])**2.)*dx[kk] -1.*a_ino/D*((ww[kk])**2.)*dy[kk]
                                                        
                    Gy[ino,kk] = -1.*b_ino/D*((ww[kk])**2.)*dx[kk] + a_ino/D*((ww[kk])**2.)*dy[kk]
                    
            int_nodes = np.linspace(0,nno_internal_nodes-1,nno_internal_nodes)
            num_neib = np.zeros(nno_internal_nodes)
            #keyboard()
            for ino in int_nodes:
                num_neib[int(ino)] = len(np.where(Gx[ino].todense()!=0)[1])
            

            # Gx_int = Gx[0:nno_internal_nodes, 0:nno_internal_nodes]
            # Gy_int = Gy[0:nno_internal_nodes, 0:nno_internal_nodes]

#for i_node in range(nno_internal_nodes):
#               for ii_node in range(nno):
#                   if ii_node in nodes_hot:
#                       b_bc[i_node] += -1.*Cx[i_node]*(Gx[i_node,ii_node]*500.) + -1.*Cy[i_node]*(Gy[i_node,ii_node]*500.)
#                   elif ii_node in nodes_cold:
#                       b_bc[i_node] += -1.*Cx[i_node]*(Gx[i_node,ii_node]*200.) + -1.*Cy[i_node]*(Gy[i_node,ii_node]*200.)
#                   else:
#                       pass
            phi_solution_n = np.zeros(nno)
            phi_solution_1 = np.zeros(nno)
            phi_solution_nn = np.zeros(nno_internal_nodes)
            phi_int_solution = 300.*np.ones(nno_internal_nodes)

            for nn in range(0, len(xy_no)):
                if nn in nodes_cold:
                    phi_solution_n[nn] = 200.
                    phi_solution_1[nn] = 200.
                elif nn in nodes_hot:
                    phi_solution_n[nn] = 500.
                    phi_solution_1[nn] = 500.
                else:
                    phi_solution_n[nn] = 300. #*np.array(np.ones(nno)) # initial values
                    phi_solution_1[nn] = 300.


            for n in range(interation):
                #keyboard()
                phi_1 = phi_int_solution + -1.*dt*((Cx_int*Gx + Cy_int*Gy).dot(phi_solution_n))

                phi_solution_1[0:nno_internal_nodes] = phi_1[0:nno_internal_nodes]
                #keyboard()
                R_1 = -1.*((Cx_int*Gx + Cy_int*Gy).dot(phi_solution_1))
                R_n = -1.*((Cx_int*Gx + Cy_int*Gy).dot(phi_solution_n))
                #phi_1 = phi_solution[:,0] + dt*(-1.*np.inner(Cx_int,Gx.dot(phi_solution[:,0]) + -1.*np.inner(Cy_int,Gy.dot(phi_solution[:,0]))))
                #R_1 = -1.*np.inner(Cx_int,Gx.dot(phi_1)) + -1.*np.inner(Cy_int,Gy.dot(phi_1))
                #R_n = -1.*np.inner(Cx_int,Gx.dot(phi_solution[:,0])) + -1.*np.inner(Cy_int,Gy.dot(phi_solution[:,0]))
                #keyboard()
                phi_int_solution_nn = phi_int_solution + 1./2.*dt*(R_n + R_1)
                phi_solution_n[0:nno_internal_nodes] = phi_int_solution_nn[0:nno_internal_nodes]
                phi_int_solution = phi_int_solution_nn
                    #for nn in range(0, len(xy_no)):
                    #if nn in nodes_cold:
                    #    phi_solution[nn,0] = 200.
                    #elif nn in nodes_hot:
                    #    phi_solution[nn,0] = 500.
                    #else:
                    #    pass


            final_solution = np.zeros(len(xy_no))
            for nn in range(0, len(xy_no)):
                if nn in nodes_cold:
                    final_solution[nn] = 200.
                elif nn in nodes_hot:
                    final_solution[nn] = 500.
                else:
                    final_solution[nn] = phi_solution[nn,0]
    
            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = phi_solution_n # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
                
            # define parameters for region(s) to be masked, initializes mask array
            circ1_cent = [1.25,0.25]
            circ2_cent = [1.25,0.75]
            circ3_cent = [1.75,0.25]
            circ4_cent = [1.75,0.75]
            circ_rad = 0.07
                    
            mask_mat = np.zeros(np.size(X)).reshape(X.shape)
                    
            # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
            for ix,zx in enumerate(X[1]):
                for jy,zy in enumerate(Y[1]):
                    dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                    dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                    dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                    dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)
                            
                    if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                        mask_mat[ix,jy]=np.nan

            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)

            figure_folder = "../report/"
            figure_name = 'P1_TEMP CONTOUR CONVECTIVE RK2_' + filename.split('.')[0]+'.jpg'  # setting up file name
    
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
            
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=500, vmin=200.)
            plt.colorbar()
            ax.set_title('TEMP CONTOUR AT DIFFERENT STAGE, RK2 (ONLY CONVECTIVE)')
            
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()


        ### Problem1 - (4) ###
        ### Crank-Nicolson method ###
        if Problem1_4:
            for n in range(interation):
                phi_solution[:,1] = np.linalg.inv(np.identity(nno_internal_nodes) + dt/2.0*A).dot((q_bc - 0.5*A.dot(phi_solution[:,0]))*dt + phi_solution[:,0])
                phi_solution[:,0] = phi_solution[:,1]

            final_solution = np.zeros(len(xy_no))
            for nn in range(0, len(xy_no)):
                if nn in nodes_cold:
                    final_solution[nn] = 200.
                elif nn in nodes_hot:
                    final_solution[nn] = 500.
                else:
                    final_solution[nn] = phi_solution[nn,1]

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = final_solution # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
            
             # define parameters for region(s) to be masked, initializes mask array
            circ1_cent = [1.25,0.25]
            circ2_cent = [1.25,0.75]
            circ3_cent = [1.75,0.25]
            circ4_cent = [1.75,0.75]
            circ_rad = 0.07
                     
            mask_mat = np.zeros(np.size(X)).reshape(X.shape)
                     
            # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
            for ix,zx in enumerate(X[1]):
                for jy,zy in enumerate(Y[1]):
                    dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                    dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                    dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                    dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)
                             
                    if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                        mask_mat[ix,jy]=np.nan
                     
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)
                                 
            figure_folder = "../report/"
            figure_name = 'P1_TEMP CONTOUR C-R_' + filename.split('.')[0]+'.jpg'  # setting up file name

            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
        
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=500, vmin=200.)
            plt.colorbar()
            ax.set_title('TEMP CONTOUR AT DFFERENT STAGE (CRANK & NICOLSON)')
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

if Problem2 :
    icemcfd_project_folder = './'
    file_vec = ['quad_v3.msh'] # mesh_c_quality_1.msh, hw2_mesh2.msh, New_mesh_v1.msh, quad_v3.msh
    
    for filename in file_vec:
        
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)


        ## Generating staggered grid via "Gauss-Lobatoo"
        N=10 # solution points
        x_flux_p = np.zeros(N+1)
        y_flux_p = np.zeros(N+1)
        x_sol_p = np.zeros(N)
        y_sol_p = np.zeros(N)
        for j in range(0,N+1):
            x_flux_p[j] = 0.5*(1.-math.cos((math.pi)/N*j))
            y_flux_p[j] = 0.5*(1.-math.cos((math.pi)/N*j))
        for jj in range(0,N):
            x_sol_p[jj] = 0.5*(1-math.cos( (2.*jj+1)/(2.*N)*math.pi) )
            y_sol_p[jj] = 0.5*(1-math.cos( (2.*jj+1)/(2.*N)*math.pi) )

        flux_p = np.zeros(((N+1)**2,2))
        sol_p = np.zeros(((N)**2,2))
        #w, h = 8, 5;
        #sol_p = [[0 for x in range(w)] for y in range(h)]

        i=0
        for rr in range(0,N+1):
            for cc in range(0,N+1):
                #keyboard()
                flux_p[i] = [x_flux_p[rr],y_flux_p[cc]]
                i+=1
        #keyboard()
        ii=0
        for rr in range(0,N):
            for cc in range(0,N):
                sol_p[ii] = [x_sol_p[rr],y_sol_p[cc]]
                ii+=1

#keyboard()

        fig_width = 30
        fig_height = 17
        textFontSize   = 15
        gcafontSize    = 32
        lineWidth      = 2
        
        Plot_Node_Labels = True
        Plot_Face_Labels = True
        Plot_CV_Labels   = True
        
        mgplx = 0.05*np.abs(max(flux_p[:,0])-min(flux_p[:,0]))
        mgply = 0.05*np.abs(max(flux_p[:,1])-min(flux_p[:,1]))
        
        fig = plt.figure(0,figsize=(fig_width,fig_height))
        ax = fig.add_subplot(111)
        ax.plot(flux_p[:,0],flux_p[:,1],'o',markersize=8,markerfacecolor='r')
        ax.plot(sol_p[:,0],sol_p[:,1],'o',markersize=8,markerfacecolor='b')

#node_color = 'k'
#       centroid_color = 'r'
        
        #       for inos_of_fa in noofa:
#   ax.plot(flux_p[inos_of_fa,0], xy_no[inos_of_fa,1], 'k-', linewidth = lineWidth)
    
        ax.axis('equal')
        
        plt.axis([-0.3, 2.2, -0.3, 1.2])
        plt.axis('off')
        
        ax.arrow(-0.2, -0.2, 0.5, 0, head_width=0.05, head_length=0.05, fc='k', ec='k')
        ax.arrow(-0.2, -0.2, 0, 0.5, head_width=0.05, head_length=0.05, fc='k', ec='k')
        ax.text(0.4, -0.2, 'x', size=35, ha='center', va='center')
        ax.text(-0.2, 0.4, 'y', size=35, ha='center', va='center')
        
        figure_folder = "../report/"
        figure_name = 'P2_GRID GENERATION_.jpg'
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.savefig(figure_file_path)
        plt.close()

#keyboard()
        f_x = np.zeros((N+1)**2)
        f_y = np.zeros((N+1)**2)
        # setting the vector field
        for iii in range(0,(N+1)**2):
            f_x[iii] = np.cos(math.pi*flux_p[iii][0])*np.sin(math.pi*flux_p[iii][1])
            f_y[iii] = -np.sin(math.pi*flux_p[iii][0])*np.cos(math.pi*flux_p[iii][1])

        # Lagrange interpolation method (row)
        n = N+1
        x=flux_p[:,0]
        y=f_x

# for i in range (0,n):
#            x.append
#            y.append
        retval=0
        for i in range(0,n):
            prod=1
            for j in range(0,n):
                if i!=j:
                    prod = prod*(ksi-x[j])*1.0/(x[i]-x[j])

            prod = prod*y[i]
            retval = retval + prod

        # Lagrange interpolation method (column)
        x2=flux_p[:,1]
        y2=f_y

        retval2=0
        for ii in range(0,n):
            prod2=1
            for jj in range(0,n):
                if ii!=jj:
                    prod2 = prod2*(ksi-x2[jj])*1.0/(x[ii]-x[jj])

            prod2 = prod2*y2[ii]
            retval2 = retval2 + prod2





keyboard()
