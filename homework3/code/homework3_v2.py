import os
import sys
import numpy as np
import scipy as sp
import scipy.sparse.linalg as splinalg
from scipy.interpolate import griddata
from scipy.sparse import csr_matrix
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
from pdb import set_trace as keyboard
import matplotlib.pyplot as plt
import matplotlib as mplt
import binascii
import numpy as N
import umesh_reader
from collections import Counter
from matplotlib.pyplot import figure, show
import numpy
from numpy import ma
import math
from sympy import Symbol, solve
import time
import matplotlib.lines as mlines
import scipy.sparse as scysparse
import bivariate_fit as fit
from numpy.linalg import inv
import scipy.ndimage
from scipy.linalg import solve
import matplotlib as mpl


##################################################################
#                                                                #
#   Jeongmin's Homework 3                                        #
#   Thanks to Prof. Scalo for all your advices for everything    #
#                                                                #
##################################################################

Problem1 = True
Problem1_2 = True
Problem1_3 = False
Problem1_4 = False
Problem2 = False


if Problem1 :
    icemcfd_project_folder = './'
    file_vec = ['hw2_mesh2.msh'] # mesh_c_quality_1.msh, hw2_mesh2.msh

    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
        
        ### Problem1 - (1) ###
        nno = len(xy_no)
        ncv = len(xy_cv)
        nfa = len(xy_fa)
        
        dt_conv = np.zeros(ncv)     # time step size for convection
        dt_diff = np.zeros(ncv)     # time step size for diffusion
        area = np.zeros(ncv)
        c_norm = np.zeros(ncv)
        
        for icv in range(ncv):
            cv_nodes = np.unique(noofa[faocv[icv]])     # nodes of the cell
            cv_faces = faocv[icv]    # faces of the cell
            cv_center = xy_cv[icv]    # center of the cell
            
            cv_nodes_xy = xy_no[cv_nodes]      # location of the nodes of the cell
            
            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            ino = len(cv_nodes_xy) # the number of nodes of the cell
            x_no = cv_nodes_xy[:,0]
            y_no = cv_nodes_xy[:,1]
            x_center = float(np.sum(x_no for x_no, y_no in cv_nodes_xy)) / ino
            y_center = float(np.sum(y_no for x_no, y_no in cv_nodes_xy)) / ino
            
            # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in cv_nodes_xy:
                dx = x_no - x_center
                dy = y_no - y_center
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
            # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
            # area by shoelace formula
            a = 0.0
            for k in range(ino):
                m = (k + 1) % ino
                a += nodes_ang[k][0]*nodes_ang[m][1]
                a -= nodes_ang[m][0]*nodes_ang[k][1]
            area[icv] = abs(a) / 2.0 # area of cell[icv]
            
            # non-trivial divergence free vector field and the norm of it
            Un = np.cos(math.pi*x_center)*np.sin(math.pi*y_center)
            Vn = -np.sin(math.pi*x_center)*np.cos(math.pi*y_center)
            c = np.array([Un, Vn])
            c_norm[icv] = np.linalg.norm(c)
            
            dt_conv[icv] = 0.85*math.sqrt(area[icv])/c_norm[icv]
        min_dt_conv = min(dt_conv)      # minimum value of dt_conv
        
        # deciding the alpha
        alpha = np.zeros(ncv)
        for icv in range(ncv):
            alpha[icv] = 0.85*area[icv]/4.0/min_dt_conv
            final_alpha = min(alpha)

        # calculating CFL and make a plot of CFL
        CFL = np.zeros(ncv)
        for icv in range(ncv):
            CFL_diff = 4.0*final_alpha*min_dt_conv/area[icv]
            CFL_conv = c_norm[icv]*min_dt_conv/math.sqrt(area[icv])
            if CFL_conv >= CFL_diff:
                CFL[icv] = CFL_conv
            else:
                CFL[icv] = CFL_diff

        # contour
        x = xy_cv[:,0] # x coordinates of data
        y = xy_cv[:,1] # y coordinates of data
        phi = CFL # values of data
        # define regular grid spatially covering input data
        n = 400
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)

        # define parameters for region(s) to be masked, initializes mask array
        circ1_cent = [1.25,0.25]
        circ2_cent = [1.25,0.75]
        circ3_cent = [1.75,0.25]
        circ4_cent = [1.75,0.75]
        circ_rad = 0.07
    
        mask_mat = np.zeros(np.size(X)).reshape(X.shape)
    
        # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
        for ix,zx in enumerate(X[1]):
            for jy,zy in enumerate(Y[1]):
                dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)

                if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                    mask_mat[ix,jy]=np.nan

        # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)

        figure_folder = "../report/"
        figure_name = 'P1_CFL_' + filename.split('.')[0]+'.jpg'  # setting up file name
        
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
        
        # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=0.85, vmin=0.)
        plt.colorbar()
        ax.set_title('CFL CONTOUR')
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

        ### Problem1 - (2) ###
        ### Explicit Euler scheme, purely diffusive problem ###

        ### Generating "gradient operator" by using a least squre prodcedure ###

        faces_cold = np.where(np.array(partofa) == 'COLD')
        faces_hot = np.where(np.array(partofa) == 'HOT')
        nodes_cold = np.unique(noofa[faces_cold])
        nodes_hot = np.unique(noofa[faces_hot])
    
        nno_int = nno - len(nodes_cold) - len(nodes_hot)

        Gx = csr_matrix((nno,nno))
        Gy = csr_matrix((nno,nno))

        # numerical procedure
        for ino in range(nno):
        
            sno = np.unique(noofa[faono[ino]]) # neighboring vertices including centroid
            index = np.where(sno == ino)
            only_sno = np.delete(sno, index) # only neighboring CVs
        
            a_ino = 0.0
            b_ino = 0.0
            c_ino = 0.0
            
            dx = np.zeros(nno)
            dy = np.zeros(nno)
            ww = np.zeros(nno)
            
            k = 0
            for xx in only_sno:
                dx[xx] = xy_no[xx,0] - xy_no[ino,0]
                dy[xx] = xy_no[xx,1] - xy_no[ino,1]
                ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
            
            
            for xxx in only_sno:
                a_ino += ((ww[xxx])**2.)*(dx[xxx])**2.
                b_ino += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                c_ino += ((ww[xxx])**2.)*(dy[xxx])**2.
            d_ino = a_ino*c_ino-(b_ino)**2.
            
            for kk in only_sno:
                Gx[ino,ino] += -1.*c_ino/d_ino*((ww[kk])**2.)*dx[kk] + b_ino/d_ino*((ww[kk])**2.)*dy[kk]
                
                Gx[ino,kk] = c_ino/d_ino*((ww[kk])**2.)*dx[kk] - b_ino/d_ino*((ww[kk])**2.)*dy[kk]
                
                Gy[ino,ino] += b_ino/d_ino*((ww[kk])**2.)*dx[kk] -1.*a_ino/d_ino*((ww[kk])**2.)*dy[kk]
                
                Gy[ino,kk] = -1.*b_ino/d_ino*((ww[kk])**2.)*dx[kk] + a_ino/d_ino*((ww[kk])**2.)*dy[kk]
        
        ### divergence construction based on HW(1) ###
        
        Dx_n2cv = csr_matrix((ncv,nno)) # Dx or Dy matrix
        Dy_n2cv = csr_matrix((ncv,nno)) # Dx or Dy matrix
    
        for icv in range(ncv) : # for example icv 128
            nodes_f = np.unique(noofa[faocv[icv]])
            face_f = faocv[icv]
            dist = np.zeros(len(xy_fa))
            final_normal_v = np.zeros((len(xy_fa),2))
            coc = xy_cv[icv] # center of cell
        
            nodes_xy = xy_no[np.unique(noofa[faocv[icv]])]
            
            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            nv = len(nodes_xy) # of corners
            x_no = nodes_xy[:,0]
            y_no = nodes_xy[:,1]
            cx = float(np.sum(x_no for x_no, y_no in nodes_xy)) / nv
            cy = float(np.sum(y_no for x_no, y_no in nodes_xy)) / nv
            # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in nodes_xy:
                dx = x_no - cx
                dy = y_no - cy
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
            # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
            # Area by shoelace formula
            area = 0.0
            for k in range(nv):
                m = (k + 1) % nv
                area += nodes_ang[k][0]*nodes_ang[m][1]
                area -= nodes_ang[m][0]*nodes_ang[k][1]
            area = abs(area) / 2.0 # area of cell[icv]
        
            for jj in nodes_f: # for example, node[0]
                twofaces_onenode = faono[jj]
                common_faces = np.intersect1d(face_f, twofaces_onenode)
                ln_x = np.zeros(len(xy_fa))
                ln_y = np.zeros(len(xy_fa))
            
            
            for j in common_faces:
                fff = len(common_faces)
                two_nodes = noofa[j]
                
                dot_11 = xy_no[two_nodes[0]]
                dot_22 = xy_no[two_nodes[1]]
                dist[j] = math.hypot( dot_22[0] - dot_11[0], dot_22[1] - dot_11[1])
                    
                # find normal vector
                vector_face = xy_no[two_nodes[0]] - xy_no[two_nodes[1]]   # vector parallel to face
                vector_nn = np.array([-vector_face[1], vector_face[0]]) # vector perpendicular to face
                norm = np.linalg.norm(vector_nn)
                normalized_unit_normal_v = vector_nn / norm
                    
                vv = coc - xy_fa[j]   # vector from face center to cell centroid
                mm = np.inner(normalized_unit_normal_v, vv)
                    
                if mm < 0:
                    final_normal_v[j] = normalized_unit_normal_v
                else:
                    final_normal_v[j] = -1.*normalized_unit_normal_v
                ln_x[j] = dist[j]*final_normal_v[j,0]  # x component of normal vector
                ln_y[j] = dist[j]*final_normal_v[j,1]  # x component of normal vector
            Dx_n2cv[icv, jj] = np.sum(ln_x) / area
            Dy_n2cv[icv, jj] = np.sum(ln_y) / area
        
        ### Building "interpolation" from CVs to nodes

        intp = csr_matrix((nno, ncv)) # sparse matrix for interpolation
        
        for iii in range(nno):
            cvs = np.unique(cvofa[faono[iii]])
            cvs = np.array([num for num in cvs if num >=0]) # remove ghost cells
            n_cvs = len(cvs) # number of control volumes surrding a node
            for bb in cvs:
                #intp[iii,bb] = 1./(np.sqrt((xy_no[iii,0]-xy_cv[bb,0])**2. + (xy_no[iii,1]-xy_cv[bb,1])**2.))
                intp[iii,bb] = 1./n_cvs

        ### Building "Laplacian operator" for only internal nodes ###
            A = -1.0*final_alpha*intp.dot(Dx_n2cv).dot(Gx) + -1.0*final_alpha*intp.dot(Dy_n2cv).dot(Gy)

        dt = min_dt_conv
        iteration = input('Enter the number of iteration:')
        phi_solution = np.zeros((nno,2))
        phi_solution[:,0] = 300.0*np.array(np.ones(nno)) # initial values

        if Problem1_2:
            for n in range(iteration):
                phi_solution[:,1] = dt*np.array(-1.*A.dot(phi_solution[:,0])) + phi_solution[:,0]
                phi_solution[:,0] = phi_solution[:,1]
        
                for nn in nodes_cold:
                    phi_solution[nn,0] = 200.
                for mm in nodes_hot:
                    phi_solution[mm,0] = 500.

                            
            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = phi_solution[:,1] # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
                
            # define parameters for region(s) to be masked, initializes mask array
            circ1_cent = [1.25,0.25]
            circ2_cent = [1.25,0.75]
            circ3_cent = [1.75,0.25]
            circ4_cent = [1.75,0.75]
            circ_rad = 0.07
                         
            mask_mat = np.zeros(np.size(X)).reshape(X.shape)
                         
            # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
            for ix,zx in enumerate(X[1]):
                for jy,zy in enumerate(Y[1]):
                    dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                    dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                    dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                    dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)
                                 
                    if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                        mask_mat[ix,jy]=np.nan

            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)
    
            figure_folder = "../report/"
            figure_name = 'P1_TEMP CONTOUR EULER_TEST_' + filename.split('.')[0]+'.jpg'  # setting up file name
        
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
            
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=500, vmin=200.)
            plt.colorbar()
            ax.set_title('TEMP CONTOUR AT DIFFERENT STAGE (EXPLICIT EULER)')
            
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
        
        
        ### Problem1 - (3) ###
        ### Explicit Euler scheme, purely convective problem ###
        if Problem1_3:
            Cx = csr_matrix(nno,nno)
            Cy = csr_matrix(nno,nno)
            A_temporary = csr_matrix(nno,nno)
            A = csr_matrix(nno_int,nno_int)
            for n in range(interation):
                for nn in range(nno):
                    Cx[nn] = np.cos(math.pi*xy_no[nn,0])*np.sin(math.pi*xy_no[nn,1])
                    Cy[nn] = -np.sin(math.pi*xy_no[nn,0])*np.cos(math.pi*xy_no[nn,1])
                

                
                phi_solution[:,1] = phi_solution[:,0] + dt*np.array(q_bc - ())

        ### Problem1 - (4) ###
        ### Crank-Nicolson method ###
        if Problem1_4:
            for n in range(iteration):
                phi_solution[:,1] = np.linalg.inv(np.identity(nno_int) + dt/2.0*A).dot((q_bc - 0.5*A.dot(phi_solution[:,0]))*dt + phi_solution[:,0])
                phi_solution[:,0] = phi_solution[:,1]

            final_solution = np.zeros(len(xy_no))
            for nn in range(0, len(xy_no)):
                if nn in nodes_cold:
                    final_solution[nn] = 200.
                elif nn in nodes_hot:
                    final_solution[nn] = 500.
                else:
                    final_solution[nn] = phi_solution[nn,1]

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = final_solution # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
            
             # define parameters for region(s) to be masked, initializes mask array
            circ1_cent = [1.25,0.25]
            circ2_cent = [1.25,0.75]
            circ3_cent = [1.75,0.25]
            circ4_cent = [1.75,0.75]
            circ_rad = 0.07
                     
            mask_mat = np.zeros(np.size(X)).reshape(X.shape)
                     
            # for loops to cycle through all points and check if they are part of masked region(s).  Updates mask_mat accordingly
            for ix,zx in enumerate(X[1]):
                for jy,zy in enumerate(Y[1]):
                    dist1 = np.sqrt((X[ix,jy]-circ1_cent[0])**2. + (Y[ix,jy]-circ1_cent[1])**2.)
                    dist2 = np.sqrt((X[ix,jy]-circ2_cent[0])**2. + (Y[ix,jy]-circ2_cent[1])**2.)
                    dist3 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ3_cent[1])**2.)
                    dist4 = np.sqrt((X[ix,jy]-circ3_cent[0])**2. + (Y[ix,jy]-circ4_cent[1])**2.)
                             
                    if dist1 < circ_rad or dist2 < circ_rad or dist3 < circ_rad or dist4 < circ_rad:
                        mask_mat[ix,jy]=np.nan
                     
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(mask_mat,Z) #np.isnan(Z)
                                 
            figure_folder = "../report/"
            figure_name = 'P1_TEMP CONTOUR C-N dT=0.01_' + filename.split('.')[0]+'.jpg'  # setting up file name

            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30
        
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=500, vmin=200.)
            plt.colorbar()
            ax.set_title('TEMP CONTOUR AT DIFFERENT STAGE (CRANK & NICOLSON)')
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()



keyboard()
