import os
import sys
import numpy as np
import scipy as sp
import scipy.sparse.linalg as splinalg
from scipy.interpolate import griddata
from scipy.sparse import csr_matrix
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
from pdb import set_trace as keyboard
import matplotlib.pyplot as plt
import matplotlib as mplt
import binascii
import numpy as N
import umesh_reader
from collections import Counter
from matplotlib.pyplot import figure, show
import numpy
from numpy import ma
import math
from sympy import Symbol, solve
import time
import matplotlib.lines as mlines
import scipy.sparse as scysparse
import bivariate_fit as fit
import test_script as test
from numpy.linalg import inv
import scipy.ndimage
from scipy.linalg import solve
import matplotlib as mpl


##############################################################
#                                                            #
# Jeongmin's Homework 2                                      #
# Thanks to Prof. Scalo for all your advices for everything  #
#                                                            #
##############################################################

Problem1 = False
Problem1_b = False
Problem2 = False
Problem3 = True

if Problem1 :
    icemcfd_project_folder = './'
    file_vec = ['quad_v3.msh']
    
    for N in range(5,9):
        #if N == 5:
        #    R = 1
        #    t = [2./5.*np.pi,4./5.*np.pi, 6./5.*np.pi, 8./5.*np.pi, 10./5.*np.pi ]
        #    xn = R*np.cos(t)
        #    yn = R*np.sin(t)
        test.Validation(N)
    

    j = 0
    Z_conv = np.zeros((4,201))
    Z_conv2 = np.zeros((4,201))


    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
        
        nno = len(xy_no)
        
        faces_cold = np.where(np.array(partofa) == 'COLD')
        faces_hot = np.where(np.array(partofa) == 'HOT')
        nodes_cold = np.unique(noofa[faces_cold])
        nodes_hot = np.unique(noofa[faces_hot])

        nno_int = nno - len(nodes_cold) - len(nodes_hot)
        A = csr_matrix((nno_int,nno_int))
        
        phi_exact = np.zeros(len(xy_no))
        d2phidx2_exact = np.zeros(len(xy_no))
        d2phidy2_exact = np.zeros(len(xy_no))
        phi_num_x = np.zeros(len(xy_no))
        phi_num_y = np.zeros(len(xy_no))
        dphidx_num = np.zeros(len(xy_no))
        dphidy_num = np.zeros(len(xy_no))
        d2phidx2_num = np.zeros(len(xy_no))
        d2phidy2_num = np.zeros(len(xy_no))
        
        # source terms setting
        b_source = np.zeros(nno_int)
        b_bc = np.zeros(nno_int)
        
        for iii in range(0, len(xy_no)):
            if ( 0.4 <= xy_no[iii,0] <= 0.6):
                if (0.3 <= xy_no[iii,1] <= 0.7):
                    b_source[iii] = 3000
    
        for i_node in range(0, nno_int):
            if not -1 in cvofa[faono[i_node]]:
                nodes_polyfit = np.unique(noofa[faono[i_node]])     # including the centroid itself
                index = np.where(nodes_polyfit == i_node)
                index_str = ''.join(str(e) for e in index)
                surrounding_nodes = np.delete(nodes_polyfit, index)     # except the centroid itslef
                
                N = len(nodes_polyfit)
                mesh_abs_tol = 0.0
                centroid_location = np.array(xy_no[i_node])
                xc = centroid_location[0]
                yc = centroid_location[1]
                
                # Location of nodes around a centroid and a center node
                xnyn = np.array(xy_no[nodes_polyfit])
                xn = xnyn[:,0]
                yn = xnyn[:,1]
                
                weights_x = np.zeros(N)
                weights_y = np.zeros(N)
                weights_dx = np.zeros(N)
                weights_dy = np.zeros(N)
                weights_dx2 = np.zeros(N)
                weights_dy2 = np.zeros(N)
                
                
                for ino in range(0, N):
                    phi_base = np.zeros(N)
                    phi_base[ino] = 1.0
                    weights_x[ino],weights_dx[ino],weights_dx2[ino] = fit.BiVarPolyFit_X(xc,yc,xn,yn,phi_base)
                    weights_y[ino],weights_dy[ino],weights_dy2[ino] = fit.BiVarPolyFit_Y(xc,yc,xn,yn,phi_base)
                    
                    weights_sum_dx2dy2 = weights_dx2 + weights_dy2
                    
                    if nodes_polyfit[ino] in nodes_cold:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ino]*300
                    elif nodes_polyfit[ino] in nodes_hot:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ino]*500
                    else:
                        A[i_node, nodes_polyfit[ino]] = weights_sum_dx2dy2[ino]

            bb = b_bc + b_source
        
        result_temp = sp.sparse.linalg.spsolve(A, bb)
        
        kkkk = np.zeros(len(xy_no))
        for pp in range(0, len(xy_no)):
            if pp in nodes_cold:
                kkkk[pp] = 300.
            elif pp in nodes_hot:
                kkkk[pp] = 500.
            else:
                kkkk[pp] = result_temp[pp]
        
        
        # spy plot
        fig = figure(figsize=(14,6))
        ax = fig.add_subplot(111)
        ax.set_title('SPARSE MATRIX A')
        ax.spy(A, precision=0.1, markersize=1)

        figure_folder = "../report/"
        figure_name = 'P1_SPARSE_MATRIX_A_' + filename.split('.')[0]+'.jpg' # change file name
    
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()


        # contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = kkkk # values of data
        # define regular grid spatially covering input data
        n = 201
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
        # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
                 
        figure_folder = "../report/"
        figure_name = 'P1_TEMP FIELD_' + filename.split('.')[0]+'.jpg'  # setting up file name
            
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
                                
        # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=500., vmin=200.)
        plt.colorbar()
        ax.set_title('TEMPERATURE CONTOUR')
                                
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

        # Grid convergence test
        Z_conv[j] = Z[100]
        Z_conv2[j] = Z[:,150]
        j += 1

    plt.gca().set_color_cycle(['red', 'green', 'blue', 'yellow'])
    plt.plot(X[100], Z_conv[0])
    plt.plot(X[100], Z_conv[1])
    plt.plot(X[100], Z_conv[2])
    plt.plot(X[100], Z_conv[3])
    plt.axis([0, 2., 0, 600])
    plt.xlabel('X')
    plt.ylabel('TEMPERATURE')
    plt.title('GRID CONVERGENCE ALONG A HORIZONTAL LINE')

    plt.legend(['A', 'B', 'C', 'D'], loc='upper left')

    figure_folder = "../report/"
    figure_name = 'P1_GRID_CONVERGENCE_CUT_1.jpg'
    figure_file_path = figure_folder + figure_name
    print "Saving figure: " + figure_file_path
    plt.tight_layout()
    plt.savefig(figure_file_path)
    plt.close()

    # Grid convergence test_2 @ different cut

    plt.gca().set_color_cycle(['red', 'green', 'blue', 'yellow'])
    plt.plot(Y[:,0], Z_conv2[0])
    plt.plot(Y[:,0], Z_conv2[1])
    plt.plot(Y[:,0], Z_conv2[2])
    plt.plot(Y[:,0], Z_conv2[3])
    plt.axis([0, 1., 0, 600])
    plt.xlabel('Y')
    plt.ylabel('TEMPERATURE')
    plt.title('GRID CONVERGENCE ALONG A VERTICAL LINE')
    
    plt.legend(['A', 'B', 'C', 'D'], loc='upper left')
    
    figure_folder = "../report/"
    figure_name = 'P1_GRID_CONVERGENCE_CUT_2.jpg'
    figure_file_path = figure_folder + figure_name
    print "Saving figure: " + figure_file_path
    plt.tight_layout()
    plt.savefig(figure_file_path)
    plt.close()

#########################################################################

if Problem1_b :
    icemcfd_project_folder = './'
    file_vec = ['circle.msh']

    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)

        faces_cold = np.where(np.array(partofa) == 'COLD')
        nodes_cold = np.unique(noofa[faces_cold])

        nno = len(xy_no) - len(nodes_cold)
        A = csr_matrix((nno,nno)) # for internal nodes

        phi_exact = np.zeros(len(xy_no))
        d2phidx2_exact = np.zeros(len(xy_no))
        d2phidy2_exact = np.zeros(len(xy_no))
        phi_num_x = np.zeros(len(xy_no))
        phi_num_y = np.zeros(len(xy_no))
        dphidx_num = np.zeros(len(xy_no))
        dphidy_num = np.zeros(len(xy_no))
        d2phidx2_num = np.zeros(len(xy_no))
        d2phidy2_num = np.zeros(len(xy_no))
    
        # source terms setting
        b_source = np.zeros(nno)
        b_source = -100.

        b_bc = np.zeros(nno)

        for i_node in range(0, len(xy_no)):
            if not -1 in cvofa[faono[i_node]]:
                nodes_polyfit = np.unique(noofa[faono[i_node]])     # including the centroid itself
                index = np.where(nodes_polyfit == i_node)
                index_str = ''.join(str(e) for e in index)
                surrounding_nodes = np.delete(nodes_polyfit, index)     # except the centroid itslef
        
                N = len(nodes_polyfit)
                mesh_abs_tol = 0.0
                centroid_location = np.array(xy_no[i_node])
                xc = centroid_location[0]
                yc = centroid_location[1]
                
                # Location of nodes around a centroid and a center node
                xnyn = np.array(xy_no[nodes_polyfit])
                xn = xnyn[:,0]
                yn = xnyn[:,1]
                
                weights_x = np.zeros(N)
                weights_y = np.zeros(N)
                weights_dx = np.zeros(N)
                weights_dy = np.zeros(N)
                weights_dx2 = np.zeros(N)
                weights_dy2 = np.zeros(N)
                
                
                for ino in range(0, N):
                    phi_base = np.zeros(N)
                    phi_base[ino] = 1.0
                    weights_x[ino],weights_dx[ino],weights_dx2[ino] = fit.BiVarPolyFit_X(xc,yc,xn,yn,phi_base)
                    weights_y[ino],weights_dy[ino],weights_dy2[ino] = fit.BiVarPolyFit_Y(xc,yc,xn,yn,phi_base)
                    
                    weights_sum_dx2dy2 = weights_dx2 + weights_dy2
                    
                    if nodes_polyfit[ino] in nodes_cold:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ino]*100
                    else:
                        A[i_node, nodes_polyfit[ino]] = weights_sum_dx2dy2[ino]
    
            bb = b_bc + b_source
        
        result_temp = sp.sparse.linalg.spsolve(A, bb)
        
        kkkk = np.zeros(len(xy_no))
        for pp in range(0, len(xy_no)):
            if pp in nodes_cold:
                kkkk[pp] = 100.
            else:
                kkkk[pp] = result_temp[pp]

        # contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = kkkk # values of data
        # define regular grid spatially covering input data
        n = 201
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
        
        # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
        
        figure_folder = "../report/"
        figure_name = 'P1_CIRCLE_NUMERICAL_' + filename.split('.')[0]+'.jpg'  # setting up file name
        
        figwidth       = 15
        figheight      = 12
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
        
        # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        #ax = fig.add_subplot(111, aspect='equal')
        #plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=800., vmin=100.)
        plt.colorbar()
        plt.title('TEMPERATURE CONTOUR (NUMERICAL)')
        #ax.set_title('TEMPERATURE CONTOUR (NUMERICAL)')
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

    # Analytic solution
        azimuths = np.radians(np.linspace(0,360, 360))
        zeniths = np.linspace(0, 5.0, 101)
        r, theta = np.meshgrid(zeniths, azimuths)
        analytic_sol = 100/4*(5**2)*(1-(r**2)/(5**2))+100

        fig, ax = plt.subplots(subplot_kw=dict(projection='polar'))
        p2 = ax.contourf(theta, r, analytic_sol)

        vmin,vmax = p2.get_clim()
        cNorm = mpl.colors.Normalize(vmin=vmin, vmax=vmax)
        ax3 = fig.add_axes([0.9, 0.1, 0.03, 0.8])
        cb1 = mpl.colorbar.ColorbarBase(ax3, norm=cNorm)

        ax.set_title('TEMPERATURE CONTOUR (ANALYTICAL)')

        figure_folder = "../report/"
        figure_name = 'P1_CIRCLE_ANALYTICAL_' + filename.split('.')[0]+'.jpg'  # setting up file name

        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

#########################################################################
#   Problem 2
#   Four different qualities for mesh c
#########################################################################

if Problem2 :
    icemcfd_project_folder = './'
    file_vec = ['mesh_c_quality_4.msh','mesh_c_quality_3.msh','mesh_c_quality_2.msh','mesh_c_quality_1.msh']
    #'mesh_c_quality_4.msh','mesh_c_quality_3.msh','mesh_c_quality_2.msh','mesh_c_quality_1.msh'
    # 'hw2_mesh2.msh' (another guy's mesh)
    
    ee = 0
    t = np.zeros(4)
    q = np.zeros(4)

    max_vals = np.zeros(4)
    nn = input('Enter the number of iteration for G-S:')

    print('The inputted iteration number is:', nn)
     #### number of iteration for Gauss-Seidel iterative scheme

    
    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
        
        faces_cold = np.where(np.array(partofa) == 'COLD')
        faces_hot = np.where(np.array(partofa) == 'HOT')
        nodes_cold = np.unique(noofa[faces_cold])
        nodes_hot = np.unique(noofa[faces_hot])
        
        nno_int = len(xy_no) - len(nodes_cold) - len(nodes_hot) # number of internal nodes
        A = csr_matrix((nno_int,nno_int)) # for internal nodes
        
        phi_exact = np.zeros(len(xy_no))
        d2phidx2_exact = np.zeros(len(xy_no))
        d2phidy2_exact = np.zeros(len(xy_no))
        phi_num_x = np.zeros(len(xy_no))
        phi_num_y = np.zeros(len(xy_no))
        dphidx_num = np.zeros(len(xy_no))
        dphidy_num = np.zeros(len(xy_no))
        d2phidx2_num = np.zeros(len(xy_no))
        d2phidy2_num = np.zeros(len(xy_no))
        
        residual = np.zeros(nn) # should be changed if number of iteration for G-S in over 150
        tol = np.zeros(nn)
        o = 0


        # source terms setting
        b_source = np.zeros(nno_int)
        b_bc = np.zeros(nno_int)
        
        # for hw2_mesh2.msh (another guy's mesh, no source term applied
        for iii in range(0, len(xy_no)):
          if ( 0.4 <= xy_no[iii,0] <= 0.6):
              if (0.3 <= xy_no[iii,1] <= 0.7):
                  b_source[iii] = 3000
        
        for i_node in range(0, nno_int):
            if not -1 in cvofa[faono[i_node]]:
                nodes_polyfit = np.unique(noofa[faono[i_node]])     # including the centroid itself
                index = np.where(nodes_polyfit == i_node)
                index_str = ''.join(str(e) for e in index)
                surrounding_nodes = np.delete(nodes_polyfit, index)     # except the centroid itslef
                
                N = len(nodes_polyfit)
                mesh_abs_tol = 0.0
                centroid_location = np.array(xy_no[i_node])
                xc = centroid_location[0]
                yc = centroid_location[1]
                
                # Location of nodes around a centroid and a center node
                xnyn = np.array(xy_no[nodes_polyfit])
                xn = xnyn[:,0]
                yn = xnyn[:,1]
                
                weights_x = np.zeros(N)
                weights_y = np.zeros(N)
                weights_dx = np.zeros(N)
                weights_dy = np.zeros(N)
                weights_dx2 = np.zeros(N)
                weights_dy2 = np.zeros(N)
                

                for ino in range(0, N):
                    phi_base = np.zeros(N)
                    phi_base[ino] = 1.0
                    weights_x[ino],weights_dx[ino],weights_dx2[ino] = fit.BiVarPolyFit_X(xc,yc,xn,yn,phi_base)
                    weights_y[ino],weights_dy[ino],weights_dy2[ino] = fit.BiVarPolyFit_Y(xc,yc,xn,yn,phi_base)
                    
                    weights_sum_dx2dy2 = weights_dx2 + weights_dy2
                
                    if nodes_polyfit[ino] in nodes_cold:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ino]*300
                    elif nodes_polyfit[ino] in nodes_hot:
                        b_bc[i_node] += -1.*weights_sum_dx2dy2[ino]*500
                    else:
                        A[i_node, nodes_polyfit[ino]] = weights_sum_dx2dy2[ino]

            bb = b_bc + b_source

        tt = time.time()

        result_temp = sp.sparse.linalg.spsolve(A, bb)
        
        t[ee] = time.time() - tt

        kkkk = np.zeros(len(xy_no))
        for pp in range(0, len(xy_no)):
            if pp in nodes_cold:
                kkkk[pp] = 300.
            elif pp in nodes_hot:
                kkkk[pp] = 500.
            else:
                kkkk[pp] = result_temp[pp]

        qq = time.time()

        # Gauss-seidel
        L = sp.sparse.tril(A)
        U = A - L
        # initial guessed values
        phi = np.zeros(nno_int)
        phi[0] = 1

        for i in range(nn):
            inv_L = sp.sparse.linalg.inv(L)
            rr = bb - U.dot(phi)
            phi = inv_L.dot(rr)
            
        # residual
            residual[o] = np.linalg.norm(A.dot(phi) - bb)
            tol[o] = residual[o]/residual[0]
            o += 1

        q[ee] = time.time() - qq


        print str(i).zfill(3)

        kkk = np.zeros(len(xy_no))
        for pp in range(0, len(xy_no)):
            if pp in nodes_cold:
                kkk[pp] = 300.
            elif pp in nodes_hot:
                kkk[pp] = 500.
            else:
                kkk[pp] = phi[pp]
            
        # residual plot
        x_axis = np.linspace(1,nn,nn)
        y_axis = tol
        plt.plot(x_axis, y_axis, linewidth=2.0)
        plt.yscale('log')

        plt.title('RESIDUAL HISTORY')
        plt.xlabel('NUMBER OF INTERATION')
        plt.ylabel('TOLERENCE')

        figure_folder = "../report/"
        figure_name = 'P2_RESIDUAL HISTORY_' + filename.split('.')[0]+ '.jpg'
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()
        
        

        # contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = kkk # values of data
        # define regular grid spatially covering input data
        n = 201
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
             np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)

        # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
        
        figure_folder = "../report/"
        figure_name = 'P2_TEMP FIELD (GAUSS SEIDEL, ITER = 20)_' + filename.split('.')[0]+'.jpg'  # setting up file name
        
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
        
        # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('TEMPERATURE CONTOUR (GAUSS SEIDEL)')
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

#######################################################################
# 3rd Question in Problem 2
#######################################################################
        B = L.dot(U)
        eigenvalues, eigenvectors = sp.sparse.linalg.eigs(B)
        max_vals[ee] = max(eigenvalues)
        ee += 1
#highest_eigen = max(eigenvalues)


########################################################################
# Problem 3
########################################################################

if Problem3 :
    icemcfd_project_folder = './'
    file_vec = ['quad_v3.msh']
    # 'mesh_b.msh','mesh_c.msh','mesh_d.msh'
    
    rank = np.zeros(4)
    rrr = 0
    for filename in file_vec:
        
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)

        ncv = len(xy_cv)
        nfa = len(xy_fa)
        nno = len(xy_no)
        GGx = csr_matrix((nno,nno))
        GGy = csr_matrix((nno,nno))

        # Analytical function
        phi = lambda X,Y: 2.*(X**2) + 5.*(Y**2)   #np.sin(Y)*np.cos(X)
        dphi_dx_dphi_dy = lambda X, Y: 4.*X + 10.*Y
        
        phi_analytic = np.zeros(len(xy_no))
        phi_derivative = np.zeros(len(xy_no))
        
        for v in range(0, len(xy_no)):
            phi_analytic[v] = phi(xy_no[v,0],xy_no[v,1])
            phi_derivative[v] =dphi_dx_dphi_dy(xy_no[v,0],xy_no[v,1])
        
        # numerical procedure
        for icv in range(nno):
        
#icv = 128
            scv = np.unique(noofa[faono[icv]]) # neighboring CVs including centroid
        # remove ghost cells
            scv = np.array([num for num in scv if num >=0])
            index = np.where(scv == icv)
            sscv = np.delete(scv, index) # only neighboring CVs
        
            a_icv = 0.0
            b_icv = 0.0
            c_icv = 0.0
        
            dx = np.zeros(nno)
            dy = np.zeros(nno)
            ww = np.zeros(nno)
        

            k = 0
            for xx in sscv:
                dx[xx] = xy_no[xx,0] - xy_no[icv,0]
                dy[xx] = xy_no[xx,1] - xy_no[icv,1]
                ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
        

            for xxx in sscv:
                a_icv += ((ww[xxx])**2.)*(dx[xxx])**2.
                b_icv += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                c_icv += ((ww[xxx])**2.)*(dy[xxx])**2.
            d_icv = a_icv*c_icv-(b_icv)**2.

            for kk in sscv:
                GGx[icv,icv] += -1.*c_icv/d_icv*((ww[kk])**2.)*dx[kk] + b_icv/d_icv*((ww[kk])**2.)*dy[kk]
            
                GGx[icv,kk] = c_icv/d_icv*((ww[kk])**2.)*dx[kk] - b_icv/d_icv*((ww[kk])**2.)*dy[kk]

                GGy[icv,icv] += b_icv/d_icv*((ww[kk])**2.)*dx[kk] -1.*a_icv/d_icv*((ww[kk])**2.)*dy[kk]

                GGy[icv,kk] = -1.*b_icv/d_icv*((ww[kk])**2.)*dx[kk] + a_icv/d_icv*((ww[kk])**2.)*dy[kk]

        
        faces_cold = np.unique(np.where(np.array(partofa) == 'COLD'))
        faces_hot = np.unique(np.where(np.array(partofa) == 'HOT'))
        nodes_cold = np.unique(noofa[faces_cold])
        nodes_hot = np.unique(noofa[faces_hot])

        ncf = np.shape(faces_cold)
        nhf = np.shape(faces_hot)
        
        # total number of internal faces
        nfa_int = len(xy_fa) - ncf[0] - nhf[0]
        
        
        phi_xf = GGx.dot(phi_analytic)
        phi_yf = GGy.dot(phi_analytic)
        sum_phi_xf_phi_yf = phi_xf + phi_yf
        
        # spy plot
        fig = figure(figsize=(14,6))
        ax = fig.add_subplot(111)
        ax.set_title('SPARSE MATRIX Gx')
        ax.spy(GGx, precision=0.1, markersize=1)
    
        figure_folder = "../report/"
        figure_name = 'P3_SPARSE_MATRIX_Gx_' + filename.split('.')[0]+'.jpg' # change file name
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

        # spy plot
        fig = figure(figsize=(14,6))
        ax = fig.add_subplot(111)
        ax.set_title('SPARSE MATRIX Gy')
        ax.spy(GGy, precision=0.1, markersize=1)

        figure_folder = "../report/"
        figure_name = 'P3_SPARSE_MATRIX_Gy_' + filename.split('.')[0]+'.jpg' # change file name
    
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()
        
        # contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = sum_phi_xf_phi_yf # values of data
        # define regular grid spatially covering input data
        n = 200
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
         # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
                     
        figure_folder = "../report/"
        figure_name = 'P3_GRADIENT CONTOUR (NUM)_' + filename.split('.')[0]+'.jpg'
         
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
         
         # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('GRADIENT FIELD_NUMERIC')
         
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()
        
        # contour
        x = xy_no[:,0] # x coordinates of data
        y = xy_no[:,1] # y coordinates of data
        phi = phi_derivative # values of data
        # define regular grid spatially covering input data
        n = 200
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
         # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
                     
        figure_folder = "../report/"
        figure_name = 'P3_GRADIENT CONTOUR (ANALYTIC)_' + filename.split('.')[0]+'.jpg'
                     
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
                     
         # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('GRADIENT FIELD_ANALYTIC')
                     
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()


###################################################################################
# Question 2 in problem 3
###################################################################################

        xx = xy_fa[:,0]
        yy = xy_fa[:,1]
       
        Un = np.zeros(nfa) # temporary vecotr matrx for all faces
        Vn = np.zeros(nfa)
        final_Uf = np.zeros(nfa_int)
        final_Vf = np.zeros(nfa_int)
    
        Dx_nfa = csr_matrix((ncv,nfa)) #temporary matrix for saving all weights
        Dy_nfa =csr_matrix((ncv,nfa))
        
        Dx_cv2f = csr_matrix((ncv,nfa_int)) # Dx or Dy matrix
        Dy_cv2f = csr_matrix((ncv,nfa_int)) # Dx or Dy matrix
        d_cv = csr_matrix((ncv,1))

        for i in range(0,nfa):
            Un[i] = np.cos(math.pi*xx[i])*np.sin(math.pi*yy[i]) #(xx[i]**2)*yy[i]
            Vn[i] = -np.sin(math.pi*xx[i])*np.cos(math.pi*yy[i]) #-(xx[i]*yy[i]**2)
            if not i in faces_cold:
                if not i in faces_hot:
                    final_Uf[i] = Un[i] # only for internal faces
                    final_Vf[i] = Vn[i]

        for icv in range(ncv):
            nodes_f = np.unique(noofa[faocv[icv]])
            faces_f = faocv[icv]
            dist = np.zeros(nfa)
            final_normal_v = np.zeros((nfa,2))
            coc = xy_cv[icv]

            nodes_xy = xy_no[nodes_f]

            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            nv = len(nodes_xy) # of corners
            x_no = nodes_xy[:,0]
            y_no = nodes_xy[:,1]
            cx = float(np.sum(x_no for x_no, y_no in nodes_xy)) / nv
            cy = float(np.sum(y_no for x_no, y_no in nodes_xy)) / nv
            
            # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in nodes_xy:
                dx = x_no - cx
                dy = y_no - cy
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
            # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
            # Area by shoelace formula
            area = 0.0
            for k in range(nv):
                m = (k + 1) % nv
                area += nodes_ang[k][0]*nodes_ang[m][1]
                area -= nodes_ang[m][0]*nodes_ang[k][1]
            area = abs(area) / 2.0 # area of cell[icv]

            for jj in nodes_f: # for example, node[0]
                twofaces_onenode = faono[jj]
                common_faces = np.intersect1d(faces_f, twofaces_onenode)
                ln_x = np.zeros(nfa)
                ln_y = np.zeros(nfa)
    
                # distance
                for j in common_faces:
                    fff = len(common_faces)
                    two_nodes = noofa[j]
        
                    dot_11 = xy_no[two_nodes[0]]
                    dot_22 = xy_no[two_nodes[1]]
                    dist[j] = math.hypot( dot_22[0] - dot_11[0], dot_22[1] - dot_11[1])

                # find normal vector
                    vector_face = xy_no[two_nodes[0]] - xy_no[two_nodes[1]]   # vector parallel to face
                    vector_nn = np.array([-vector_face[1], vector_face[0]]) # vector perpendicular to face
                    norm = np.linalg.norm(vector_nn)
                    normalized_unit_normal_v = vector_nn / norm
        
                    vv = coc - xy_fa[j]   # vector from face center to cell centroid
                    mm = np.inner(normalized_unit_normal_v, vv)
        
                    if mm < 0:
                        final_normal_v[j] = normalized_unit_normal_v
                    else:
                        final_normal_v[j] = -1.*normalized_unit_normal_v

                    ln_x[j] = dist[j]*final_normal_v[j,0]  # x component of normal vector
                    ln_y[j] = dist[j]*final_normal_v[j,1]  # x component of normal vector
                    Dx_nfa[icv, j] = ln_x[j] / area
                    Dy_nfa[icv, j] = ln_y[j] / area

        for t in range(0, nfa_int):
            Dx_cv2f[:,t] = Dx_nfa[:,t]
            Dy_cv2f[:,t] = Dy_nfa[:,t]

        q_bc = np.zeros(ncv)
        for icv in range(0, ncv):
            for tt in faces_cold:
                q_bc[icv] += Dx_nfa[icv, tt]*Un[tt] + Dy_nfa[icv, tt]*Vn[tt]
            for ttt in faces_hot:
                q_bc[icv] += Dx_nfa[icv, ttt]*Un[ttt] + Dy_nfa[icv, ttt]*Vn[ttt]

        d_cv = Dx_cv2f.dot(final_Uf) + Dy_cv2f.dot(final_Vf) + q_bc

        # graph
        fig3 = figure(figsize=(14,6))
        ax1 = fig3.add_subplot(121)
        ax2 = fig3.add_subplot(122)
        ax1.set_title('DIV, X COMPONENT OPERATORS')
        ax2.set_title('DIV, Y COMPONENT OPERATORS')

        ax1.spy(Dx_cv2f, precision=0.1, markersize=2)
        ax2.spy(Dy_cv2f, precision=0.1, markersize=2)
        
        figure_folder = "../report/"
        figure_name = 'P3_DIV OPERATOR GRAPH_' + filename.split('.')[0]+'.jpg'  # change file name
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

        # contour
        x = xy_cv[:,0] # x coordinates of data
        y = xy_cv[:,1] # y coordinates of data
        phi = d_cv # values of data
        # define regular grid spatially covering input data
        n = 200
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
        # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
            
        figure_folder = "../report/"
        figure_name = 'P3_DIV CONTOUR_' + filename.split('.')[0]+'.jpg'
                
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
                                    
        # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('DIVERGNECE CONTOUR')
            
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()


#################################################################################
# Question 3 in Problem 3
#################################################################################

        U_perturbed = np.zeros(nfa_int)
        V_perturbed = np.zeros(nfa_int)
        d_cv_star = np.zeros(ncv)
        #final_Uf = np.zeros(nfa_int)
        #final_Vf = np.zeros(nfa_int)

        for i in range(0,nfa_int):
            U_perturbed[i] = np.cos(math.pi*xx[i])*np.sin(math.pi*yy[i]) + 0.1*xx[i] + 0.1*yy[i] #(xx[i]**2)*yy[i]
            V_perturbed[i] = -np.sin(math.pi*xx[i])*np.cos(math.pi*yy[i]) + 0.1*xx[i] + 0.1*yy[i] #-(xx[i]*yy[i]**2)
            
            #if not i in faces_cold:
            #   if not i in faces_hot:
            #       final_Uf[i] = U_perturbed[i] # only for internal faces
            #       final_Vf[i] = V_perturbed[i]

        d_cv_star = Dx_cv2f.dot(U_perturbed) + Dy_cv2f.dot(V_perturbed) + q_bc

        A = Dx_cv2f.dot(GGx) + Dy_cv2f.dot(GGy)
        AA = A.todense()
        rank[rrr] = np.linalg.matrix_rank(AA)
        rrr +=1
        print(rank)
        det = np.linalg.det(AA)

#################################################################################
# Question 4 in problem 3
#################################################################################
        # sum of one of rows is equal to zero
        A[9] = 1
        d_cv_star[9] = 0

        phi_cv = sp.sparse.linalg.spsolve(A, d_cv_star)

        U_f_tilde = U_perturbed - final_Gx.dot(phi_cv)
        V_f_tilde = V_perturbed - final_Gy.dot(phi_cv)

        x_fa_int = np.zeros(nfa_int) # the position of internal faces
        y_fa_int = np.zeros(nfa_int)
        for yyy in range(0, nfa_int):
            x_fa_int[yyy] = xy_fa[yyy,0]
            y_fa_int[yyy] = xy_fa[yyy,1]
        
        
        plt.title('Arrows scale with plot width, not view')
        plt.quiver(x_fa_int, y_fa_int, U_perturbed, V_perturbed, units='width')
        plt.quiver(x_fa_int, y_fa_int, U_f_tilde, V_f_tilde, units='width', color='r')
        plt.quiver(x_fa_int, y_fa_int, final_Uf, final_Vf, units='width', color='Y')
        plt.legend(['V_STAR','V_TILDE','V_ANALYTIC'], loc='upper left')


        figure_folder = "../report/"
        figure_name = 'P3_QUIVER PLOT_' + filename.split('.')[0]+'.pdf' # change file name
        
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()

        # contour
        x = xy_cv[:,0] # x coordinates of data
        y = xy_cv[:,1] # y coordinates of data
        phi = phi_cv # values of data
        # define regular grid spatially covering input data
        n = 200
        xg = np.linspace(x.min(),x.max(),n)
        yg = np.linspace(y.min(),y.max(),n)
        X,Y = np.meshgrid(xg,yg)
        # interpolate Z values on defined grid
        Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
         # mask nan values, so they will not appear on plot
        Zm = np.ma.masked_where(np.isnan(Z),Z)
                     
        figure_folder = "../report/"
        figure_name = 'P3_PHI_CV_' + filename.split('.')[0]+'.jpg'
                     
        figwidth       = 18
        figheight      = 6
        lineWidth      = 3
        textFontSize   = 28
        gcafontSize    = 30
                     
                     # plot
        fig = plt.figure(0,figsize=(figwidth,figheight))
        ax = fig.add_subplot(111, aspect='equal')
        plt.axes(ax)
        plt.pcolormesh(X,Y,Zm,shading='gouraud')
        plt.colorbar()
        ax.set_title('PHI_cv CONTOUR')
                     
        figure_file_path = figure_folder + figure_name
        print "Saving figure: " + figure_file_path
        plt.tight_layout()
        plt.savefig(figure_file_path)
        plt.close()



keyboard()
