import os
import sys
import numpy as np
import scipy as sp
import scipy.sparse.linalg as splinalg
from scipy.interpolate import griddata
from scipy.sparse import csr_matrix
import pylab as plt
from matplotlib import rc as matplotlibrc
from pdb import set_trace
from pdb import set_trace as keyboard
import matplotlib.pyplot as plt
import matplotlib as mplt
import binascii
import numpy as N
import umesh_reader
from collections import Counter
from matplotlib.pyplot import figure, show
import numpy
from numpy import ma
import math
from sympy import Symbol, solve
import time
import matplotlib.lines as mlines
import scipy.sparse as scysparse
import bivariate_fit as fit
from numpy.linalg import inv
import scipy.ndimage
from scipy.linalg import solve
import matplotlib as mpl


##################################################################
#                                                                #
#   Jeongmin's Final Project                                     #
#   Thanks to Prof. Scalo for all your advices for everything    #
#                                                                #
##################################################################

Problem1 = True
Problem1_1 = True


if Problem1 :
    icemcfd_project_folder = './'
    file_vec = ['validation_quad.msh']
    #'project_v2_coarse.msh','project_v2_medium.msh','project_v2_fine.msh'
    graph = 0
    ZZZy = np.zeros((3,400))
    ZZZx = np.zeros((3,400))
    ZZZt = np.zeros((3,400))


    for filename in file_vec:
    
        mshfile_fullpath = icemcfd_project_folder + filename
        part_names, xy_no, xy_fa, xy_cv, noofa, cvofa, faono, faocv, partofa = umesh_reader.read_unstructured_grid(mshfile_fullpath,node_reordering=True)
        
        face_wall = np.squeeze(np.array(np.where(np.array(partofa) == 'WALL')))
        face_inlet = np.squeeze(np.array(np.where(np.array(partofa) == 'HOT')))
        face_outlet = np.squeeze(np.array(np.where(np.array(partofa) == 'COLD')))
        #face_wall = np.concatenate((face_1, face_2, face_3, face_4),axis=0)
        face_top = np.squeeze(np.array(np.where(np.array(partofa) == 'LID')))
        
        node_wall = np.unique(noofa[face_wall])
        node_inlet = np.unique(noofa[face_inlet])
        node_outlet = np.unique(noofa[face_outlet])
        node_top = np.unique(noofa[face_top])
        
        # remove duplicaiton
        node_duplicate1 = np.intersect1d(node_wall,node_outlet)
        node_duplicate2 = np.intersect1d(node_top,node_outlet)
        node_duplicate3 = np.intersect1d(node_wall,node_inlet)
        node_duplicate4 = np.intersect1d(node_top,node_inlet)
        
        node_outlet = np.setdiff1d(node_outlet, node_duplicate1)
        node_outlet = np.setdiff1d(node_outlet, node_duplicate2)
        node_inlet = np.setdiff1d(node_inlet, node_duplicate3)
        node_inlet = np.setdiff1d(node_inlet, node_duplicate4)
        
        
        nno = len(xy_no)
        ncv = len(xy_cv)
        nfa = len(xy_fa)
        nno_int = nno-len(node_wall)-len(node_top)-len(node_inlet)-len(node_outlet)
        nfa_int = nfa-len(face_wall)-len(face_top)-len(face_inlet)-len(node_outlet)

        node_internal = np.linspace(0,nno_int-1,nno_int)
        
#########################################################################
#   Boundary conditions, viscosity, time-step size                      #
#########################################################################
        
        u_top = np.array([-1.0, 0.0])
        u_inlet = np.array([0.0, 0.0])
        u_outlet = np.array([0.0, 0.0])
        u_wall = np.array([0.0, 0.0])
        u_n = np.zeros((nno_int,2))
        nu = 0.001 # viscosity
        
        t_outlet = 250
        t_top = 250
        t_wall = 250
        t_inlet = 350
        phi_nn = np.zeros(nno_int)
        phi_n = 290.0*np.array(np.ones(nno_int))
        alpha = 0.001
        
#########################################################################
#   CFL                                                                 #
#########################################################################

        dt_conv = np.zeros(ncv)     # time step size for convection
        dt_diff = np.zeros(ncv)
        dt_min = np.zeros(ncv)

        area = np.zeros(ncv)
        for icv in range(ncv):
            cv_nodes = np.unique(noofa[faocv[icv]])     # nodes of the cell
            cv_faces = faocv[icv]    # faces of the cell
            cv_center = xy_cv[icv]    # center of the cell
            
            cv_nodes_xy = xy_no[cv_nodes]      # location of the nodes of the cell
            
            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            ino = len(cv_nodes_xy) # the number of nodes of the cell
            x_no = cv_nodes_xy[:,0]
            y_no = cv_nodes_xy[:,1]
            x_center = float(np.sum(x_no for x_no, y_no in cv_nodes_xy)) / ino
            y_center = float(np.sum(y_no for x_no, y_no in cv_nodes_xy)) / ino
            
            # create a new list of vertices which includes angles
            nodes_ang = []
            for x_no, y_no in cv_nodes_xy:
                dx = x_no - x_center
                dy = y_no - y_center
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                nodes_ang.append((x_no, y_no, angle))
            # sort it using the angles
            nodes_ang.sort(key =lambda tup: tup[2])
            
            # area by shoelace formula
            a = 0.0
            for k in range(ino):
                m = (k + 1) % ino
                a += nodes_ang[k][0]*nodes_ang[m][1]
                a -= nodes_ang[m][0]*nodes_ang[k][1]
            area[icv] = abs(a) / 2.0 # area of cell[icv]
        
        c_norm = u_top[0]
        final_alpha = nu
        
        CFL = 0.85 #np.zeros(ncv)
        for icv in range(ncv):
            dt_diff = CFL*area[icv]/4./nu
            dt_conv = CFL*math.sqrt(area[icv])/abs(u_top[0])
            if dt_diff < dt_conv:
                dt_min[icv] = dt_diff
            else:
                dt_min[icv] = dt_conv
        dt = min(dt_min)
        print(dt)

##############################################################################
#   Bivariate polynomial                                                     #
##############################################################################

        B = csr_matrix((nno_int,nno_int))
        q_bc = np.zeros((nno_int,2))
        t_bc = np.zeros(nno_int)

        for i_node in range(nno_int):
            if not -1 in cvofa[faono[i_node]]:
                node_bivariate = np.unique(noofa[faono[i_node]])
                nno_bivariate = len(node_bivariate)
                horizontal_dist_1 = 1
                horizontal_dist_3 = 1
                vertical_dist_1 = 1
                vertical_dist_3 = 1
                
                for ee in node_bivariate:
                    # Find North / south vertex
                    vertical_dist = xy_no[ee][1] - xy_no[i_node][1]
                    if vertical_dist > 0 :
                        horizontal_dist_2 = abs(xy_no[ee][0] - xy_no[i_node][0])
                        if horizontal_dist_2 < horizontal_dist_1:
                            horizontal_dist_1 = horizontal_dist_2
                            north_vertex = ee
                    if vertical_dist < 0:
                        horizontal_dist_4 = abs(xy_no[ee][0] - xy_no[i_node][0])
                        if horizontal_dist_4 < horizontal_dist_3:
                            horizontal_dist_3 = horizontal_dist_4
                            south_vertex = ee

                    # East / west
                    horizontal_dist = xy_no[ee][0] - xy_no[i_node][0]
                    if horizontal_dist > 0:
                        vertical_dist_2 = abs(xy_no[ee][1] - xy_no[i_node][1])
                        if vertical_dist_2 < vertical_dist_1:
                            vertical_dist_1 = vertical_dist_2
                            east_vertex = ee
                    if horizontal_dist < 0:
                        vertical_dist_4 = abs(xy_no[ee][1] - xy_no[i_node][1])
                        if vertical_dist_4 <vertical_dist_3:
                            vertical_dist_3 = vertical_dist_4
                            west_vertex = ee
                                
                node_bivariate = np.array([i_node, west_vertex, east_vertex, south_vertex, north_vertex])
                nno_bivariate = len(node_bivariate)

                mesh_abs_tol = 0.0
                centroid = np.array(xy_no[i_node])
                xc = centroid[0]
                yc = centroid[1]

                xnyn = np.array(xy_no[node_bivariate])
                xn = xnyn[:,0]
                yn = xnyn[:,1]

                weights_x = np.zeros(nno_bivariate)
                weights_y = np.zeros(nno_bivariate)
                weights_dx = np.zeros(nno_bivariate)
                weights_dy = np.zeros(nno_bivariate)
                weights_dx2 = np.zeros(nno_bivariate)
                weights_dy2 = np.zeros(nno_bivariate)

                for ii_node in range(nno_bivariate):
                    phi_base = np.zeros(nno_bivariate)
                    phi_base[ii_node] = 1.0
                    weights_x[ii_node],weights_dx[ii_node],weights_dx2[ii_node] = fit.BiVarPolyFit_X(xc,yc,xn,yn,phi_base)
                    weights_y[ii_node],weights_dy[ii_node],weights_dy2[ii_node] = fit.BiVarPolyFit_Y(xc,yc,xn,yn,phi_base)
                    weights_sum_dx2dy2 = np.array(weights_dx2 + weights_dy2)

                    if node_bivariate[ii_node] in node_wall:
                        q_bc[i_node] += weights_sum_dx2dy2[ii_node]*u_wall
                        t_bc[i_node] += weights_sum_dx2dy2[ii_node]*t_wall
                    elif node_bivariate[ii_node] in node_top:
                        q_bc[i_node] += weights_sum_dx2dy2[ii_node]*u_top
                        t_bc[i_node] += weights_sum_dx2dy2[ii_node]*t_top
                    elif node_bivariate[ii_node] in node_inlet:
                        q_bc[i_node] += weights_sum_dx2dy2[ii_node]*u_inlet
                        t_bc[i_node] += weights_sum_dx2dy2[ii_node]*t_inlet
                    elif node_bivariate[ii_node] in node_outlet:
                        q_bc[i_node] += weights_sum_dx2dy2[ii_node]*u_outlet
                        t_bc[i_node] += weights_sum_dx2dy2[ii_node]*t_outlet
                    else:
                        B[i_node, node_bivariate[ii_node]] = weights_sum_dx2dy2[ii_node]
                            #keyboard()

##############################################################################
#   Divergence operator (for u_star)                                         #
##############################################################################

        Dx_f2cv = csr_matrix((ncv,nfa_int)) # Divergence X matrix
        Dy_f2cv = csr_matrix((ncv,nfa_int)) # Divergence Y matrix
        
        Div_u_star = csr_matrix((ncv,1))
        q_bc_div = np.zeros(ncv)
        
        for icv in range(ncv):
            node_div = np.unique(noofa[faocv[icv]])
            face_div = faocv[icv]
            dist = np.zeros(nfa)
            final_normal_v = np.zeros((nfa,2))
            cocv = xy_cv[icv] # x,y location of control volume
            
            # calculate centroid of the polygon to sort vertices in counter-clockwise direction
            vertices = xy_no[node_div]
            nve = len(vertices)
            x_ver = vertices[:0]
            y_ver = vertices[:,1]
            x_center = float(np.sum(x_ver for x_ver, y_ver in vertices)) / nve
            y_center = float(np.sum(y_ver for x_ver, y_ver in vertices)) / nve
                
            # create a new list of vertices which includes angles and sort them based on angle information
            node_ang = []
            for x_ver, y_ver in vertices:
                dx = x_ver - x_center
                dy = y_ver - y_center
                angle = (math.atan2(dy, dx) + 2.0*math.pi) % (2.0*math.pi)
                node_ang.append((x_ver, y_ver, angle))
            node_ang.sort(key =lambda tup: tup[2])
                
            # Area by shoelace formula
            area = 0.0
            for k in range(nve):
                m = (k+1) % nve
                area += node_ang[k][0]*node_ang[m][1]
                area -= node_ang[m][0]*node_ang[k][1]
            area = abs(area) / 2.0
            
            for jj in node_div:
                twofaces_onenode = faono[jj]
                common_faces = np.intersect1d(face_div, twofaces_onenode)
                
                    
                # distance between two nodes
                for j in common_faces:
                    ncf = len(common_faces)
                    twonodes = noofa[j]
                        
                    dot_11 = xy_no[twonodes[0]]
                    dot_22 = xy_no[twonodes[1]]
                    dist[j] = math.hypot( dot_22[0] - dot_11[0], dot_22[1] - dot_11[1])
                        
                    # find normal vector
                    vector_parallel = xy_no[twonodes[0]] - xy_no[twonodes[1]]   # vector parallel to face
                    vector_normal = np.array([-vector_parallel[1], vector_parallel[0]]) # vector perpendicular to face
                    norm = np.linalg.norm(vector_normal)
                    normalized_unit_normal_v = vector_normal / norm
                        
                    vv = cocv - xy_fa[j]   # vector from face center to cell centroid
                    mm = np.inner(normalized_unit_normal_v, vv)
                        
                    if mm < 0:
                        final_normal_v[j] = normalized_unit_normal_v
                    else:
                        final_normal_v[j] = -1.*normalized_unit_normal_v
                        
                    ln_x = dist[j]*final_normal_v[j,0]  # x component of normal vector
                    ln_y = dist[j]*final_normal_v[j,1]  # x component of normal vector
                    
                    Dx_nfa = ln_x / area
                    Dy_nfa = ln_y / area
                    
                    if  j in face_wall:
                        q_bc_div[icv] += Dx_nfa*u_wall[0] + Dy_nfa*u_wall[1]
                    elif j in face_top:
                        q_bc_div[icv] += Dx_nfa*u_top[0] + Dy_nfa*u_top[1]
                    elif j in face_inlet:
                        q_bc_div[icv] += Dx_nfa*u_inlet[0] + Dy_nfa*u_inlet[1]
                    elif j in face_outlet:
                        q_bc_div[icv] += Dx_nfa*u_outlet[0] + Dy_nfa*u_outlet[1]
                    else:
                        Dx_f2cv[icv,j] = Dx_nfa
                        Dy_f2cv[icv,j] = Dy_nfa



##############################################################################
#   Gradient operator (nno_int x nno int) for convective term                #
#   to get u_star                                                            #
##############################################################################

        temporary_Gx_conv = csr_matrix((nno_int,nno))
        temporary_Gy_conv = csr_matrix((nno_int,nno))
        Gx_conv = csr_matrix((nno_int,nno_int))
        Gy_conv = csr_matrix((nno_int,nno_int))

        for ino in range(nno_int):
            sno = np.unique(noofa[faono[ino]]) # neighboring vertices including centroid
            #esno = np.unique(noofa[np.unique(np.concatenate(faono[sno], axis=0))])
    
            index = np.where(sno == ino)
            only_sno = np.delete(sno, index) # only neighboring CVs
        
        #for tt in only_sno:
        #   vector_1 = xy_no[ino] - xy_no[tt]
        #   inner_dot = vector_1[0]*Cx[ino]+vector_1[1]*Cy[ino]
        #   if inner_dot >=0:
        #       pass
        #           else:
        #               index2 = np.where(only_sno == tt)
        #               only_sno = np.delete(only_sno, index2)
        
        
        
            a_ino = 0.0
            b_ino = 0.0
            c_ino = 0.0
                
            dx = np.zeros(nno)
            dy = np.zeros(nno)
            ww = np.zeros(nno)
                
            k = 0
            for xx in only_sno:
                dx[xx] = xy_no[xx,0] - xy_no[ino,0]
                dy[xx] = xy_no[xx,1] - xy_no[ino,1]
                ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
                
            for xxx in only_sno:
                a_ino += ((ww[xxx])**2.)*(dx[xxx])**2.
                b_ino += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                c_ino += ((ww[xxx])**2.)*(dy[xxx])**2.
            D = a_ino*c_ino-(b_ino)**2.
            if D==0:
                keyboard()
            
            weights_Gx_conv = np.zeros(len(only_sno)) # 0.0
            weights_Gy_conv = np.zeros(len(only_sno)) # 0.0
            w = 0
                #keyboard()
            for kk in only_sno:
                weights_Gx_conv[w] = -1.*c_ino/D*((ww[kk])**2.)*dx[kk] + b_ino/D*((ww[kk])**2.)*dy[kk]
                
                temporary_Gx_conv[ino,kk] = c_ino/D*((ww[kk])**2.)*dx[kk] - b_ino/D*((ww[kk])**2.)*dy[kk]
                
                weights_Gy_conv[w] = b_ino/D*((ww[kk])**2.)*dx[kk] -1.*a_ino/D*((ww[kk])**2.)*dy[kk]
                
                temporary_Gy_conv[ino,kk] = -1.*b_ino/D*((ww[kk])**2.)*dx[kk] + a_ino/D*((ww[kk])**2.)*dy[kk]
            
                w += 1
            
            temporary_Gx_conv[ino,ino] = np.sum(weights_Gx_conv)
            temporary_Gy_conv[ino,ino] = np.sum(weights_Gy_conv)
            
        for r in range(0, nno_int):
            Gx_conv[:,r] = temporary_Gx_conv[:,r]
            Gy_conv[:,r] = temporary_Gx_conv[:,r]
            
        q_bc_dudx_conv = np.zeros(nno_int)
        q_bc_dudy_conv = np.zeros(nno_int)
        q_bc_dvdx_conv = np.zeros(nno_int)
        q_bc_dvdy_conv = np.zeros(nno_int)


        for ino in range(0,nno_int):
            for qq in node_wall:
                q_bc_dudx_conv[ino] += temporary_Gx_conv[ino,qq]*u_wall[0]
                q_bc_dudy_conv[ino] += temporary_Gy_conv[ino,qq]*u_wall[0]
                q_bc_dvdx_conv[ino] += temporary_Gx_conv[ino,qq]*u_wall[1]
                q_bc_dvdy_conv[ino] += temporary_Gy_conv[ino,qq]*u_wall[1]


                #q_bc_conv[ino] += np.inner(u_n[ino][0],(temporary_Gx_conv[ino,qq]*u_hot)) + np.inner(u_n[ino][1], (temporary_Gy_conv[ino,qq]*u_hot))
#t_bc_conv[ino] += u_n[ino][0]*temporary_Gx_conv[ino,qq]*t_hot + u_n[ino][1]*temporary_Gy_conv[ino,qq]*t_hot
            for qq in node_top:
                q_bc_dudx_conv[ino] += temporary_Gx_conv[ino,qq]*u_top[0]
                q_bc_dudy_conv[ino] += temporary_Gy_conv[ino,qq]*u_top[0]
                q_bc_dvdx_conv[ino] += temporary_Gx_conv[ino,qq]*u_top[1]
                q_bc_dvdy_conv[ino] += temporary_Gy_conv[ino,qq]*u_top[1]
                
                #q_bc_conv[ino] += np.inner(u_n[ino][0],(temporary_Gx_conv[ino,qq]*u_top)) + np.inner(u_n[ino][1], (temporary_Gy_conv[ino,qq]*u_top))
#t_bc_conv[ino] += u_n[ino][0]*temporary_Gx_conv[ino,qq]*t_top + u_n[ino][1]*temporary_Gy_conv[ino,qq]*t_top
            for qq in node_inlet:
                q_bc_dudx_conv[ino] += temporary_Gx_conv[ino,qq]*u_inlet[0]
                q_bc_dudy_conv[ino] += temporary_Gy_conv[ino,qq]*u_inlet[0]
                q_bc_dvdx_conv[ino] += temporary_Gx_conv[ino,qq]*u_inlet[1]
                q_bc_dvdy_conv[ino] += temporary_Gy_conv[ino,qq]*u_inlet[1]
                    

                #q_bc_conv[ino] += np.inner(u_n[ino][0],(temporary_Gx_conv[ino,qq]*u_inlet)) + np.inner(u_n[ino][1], (temporary_Gy_conv[ino,qq]*u_inlet))
#t_bc_conv[ino] += u_n[ino][0]*temporary_Gx_conv[ino,qq]*t_inlet + u_n[ino][1]*temporary_Gy_conv[ino,qq]*t_inlet
            for qq in node_outlet:
                q_bc_dudx_conv[ino] += temporary_Gx_conv[ino,qq]*u_outlet[0]
                q_bc_dudy_conv[ino] += temporary_Gy_conv[ino,qq]*u_outlet[0]
                q_bc_dvdx_conv[ino] += temporary_Gx_conv[ino,qq]*u_outlet[1]
                q_bc_dvdy_conv[ino] += temporary_Gy_conv[ino,qq]*u_outlet[1]
                

                #q_bc_conv[ino] += np.inner(u_n[ino][0],(temporary_Gx_conv[ino,qq]*u_outlet)) + np.inner(u_n[ino][1], (temporary_Gy_conv[ino,qq]*u_outlet))
                #t_bc_conv[ino] += u_n[ino][0]*temporary_Gx_conv[ino,qq]*t_outlet + u_n[ino][1]*temporary_Gy_conv[ino,qq]*t_outlet



##############################################################################
#   Gradient operator (nno_int x nno int) for convective term                #
#   in energy equation                                                       #
##############################################################################

        temporary_Gx_conv_t = csr_matrix((nno_int,nno))
        temporary_Gy_conv_t = csr_matrix((nno_int,nno))
        Gx_conv_t = csr_matrix((nno_int,nno_int))
        Gy_conv_t = csr_matrix((nno_int,nno_int))

        for ino in range(nno_int):
            sno = np.unique(noofa[faono[ino]]) # neighboring vertices including centroid
            #esno = np.unique(noofa[np.unique(np.concatenate(faono[sno], axis=0))])
    
            index = np.where(sno == ino)
            only_sno = np.delete(sno, index) # only neighboring CVs
        
        #for tt in only_sno:
        #   vector_1 = xy_no[ino] - xy_no[tt]
        #   inner_dot = vector_1[0]*Cx[ino]+vector_1[1]*Cy[ino]
        #   if inner_dot >=0:
        #       pass
        #           else:
        #               index2 = np.where(only_sno == tt)
        #               only_sno = np.delete(only_sno, index2)
        
        
        
            a_ino = 0.0
            b_ino = 0.0
            c_ino = 0.0
            
            dx = np.zeros(nno)
            dy = np.zeros(nno)
            ww = np.zeros(nno)
            
            k = 0
            for xx in only_sno:
                dx[xx] = xy_no[xx,0] - xy_no[ino,0]
                dy[xx] = xy_no[xx,1] - xy_no[ino,1]
                ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
            
            for xxx in only_sno:
                a_ino += ((ww[xxx])**2.)*(dx[xxx])**2.
                b_ino += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                c_ino += ((ww[xxx])**2.)*(dy[xxx])**2.
            D = a_ino*c_ino-(b_ino)**2.
            if D==0:
                keyboard()

            weights_Gx_conv_t = np.zeros(len(only_sno)) # 0.0
            weights_Gy_conv_t = np.zeros(len(only_sno)) # 0.0
            w = 0
            #keyboard()
            for kk in only_sno:
                weights_Gx_conv_t[w] = -1.*c_ino/D*((ww[kk])**2.)*dx[kk] + b_ino/D*((ww[kk])**2.)*dy[kk]
                
                temporary_Gx_conv_t[ino,kk] = c_ino/D*((ww[kk])**2.)*dx[kk] - b_ino/D*((ww[kk])**2.)*dy[kk]
                
                weights_Gy_conv_t[w] = b_ino/D*((ww[kk])**2.)*dx[kk] -1.*a_ino/D*((ww[kk])**2.)*dy[kk]
                
                temporary_Gy_conv_t[ino,kk] = -1.*b_ino/D*((ww[kk])**2.)*dx[kk] + a_ino/D*((ww[kk])**2.)*dy[kk]
                
                w += 1
            
            temporary_Gx_conv_t[ino,ino] = np.sum(weights_Gx_conv_t)
            temporary_Gy_conv_t[ino,ino] = np.sum(weights_Gy_conv_t)

        for r in range(0, nno_int):
            Gx_conv_t[:,r] = temporary_Gx_conv_t[:,r]
            Gy_conv_t[:,r] = temporary_Gy_conv_t[:,r]
        
        t_bc_x_conv = np.zeros(nno_int)
        t_bc_y_conv = np.zeros(nno_int)
        
        for ino in range(0,nno_int):
            for qq in node_wall:
                t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_wall
                t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_wall
            for qq in node_top:
                t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_top
                t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_top
            for qq in node_inlet:
                t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_inlet
                t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_inlet
            for qq in node_outlet:
                t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_outlet
                t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_outlet



##############################################################################
#   Laplacian operator (for pressure correction)                             #
##############################################################################

##############################################################################
#   Gradient operator (ncv x ncv) for pressure correction                    #
##############################################################################

        temporary_Gx = csr_matrix((ncv,ncv))
        temporary_Gy = csr_matrix((ncv,ncv))

        for icv in range(ncv):
            # neighboring CVs including centroid
            scv = np.unique(cvofa[faocv[icv]])
            # remove ghost cells
            scv = np.array([num for num in scv if num >=0])
            index = np.where(scv == icv)
            sscv = np.delete(scv, index) # only neighboring CVs
        
            # stencil at corners
            if len(sscv) == 1:
                stencil = np.unique(cvofa[faocv[sscv[0]]])
                index_2 = np.where( stencil == icv)
                sscv = np.delete(stencil, index_2)
                
            a_icv = 0.0
            b_icv = 0.0
            c_icv = 0.0
                
            dx = np.zeros(ncv)
            dy = np.zeros(ncv)
            ww = np.zeros(ncv)
                
                
            k = 0
            for xx in sscv:
                dx[xx] = xy_cv[xx,0] - xy_cv[icv,0]
                dy[xx] = xy_cv[xx,1] - xy_cv[icv,1]
                ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
                
                
            for xxx in sscv:
                a_icv += ((ww[xxx])**2.)*(dx[xxx])**2.
                b_icv += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                c_icv += ((ww[xxx])**2.)*(dy[xxx])**2.
            D = a_icv*c_icv-(b_icv)**2.
            if D == 0:
                keyboard()


            weights_Gx_pressure = np.zeros(len(sscv)) # 0.0
            weights_Gy_pressure = np.zeros(len(sscv)) # 0.0
            www = 0

            for kk in sscv:
                weights_Gx_pressure[www] = -1.*c_icv/D*((ww[kk])**2.)*dx[kk] + b_icv/D*((ww[kk])**2.)*dy[kk]
#                temporary_Gx[icv,icv] += -1.*c_icv/D*((ww[kk])**2.)*dx[kk] + b_icv/D*((ww[kk])**2.)*dy[kk]
                
                temporary_Gx[icv,kk] = c_icv/D*((ww[kk])**2.)*dx[kk] - b_icv/D*((ww[kk])**2.)*dy[kk]
 
                weights_Gy_pressure[www] = b_icv/D*((ww[kk])**2.)*dx[kk] -1.*a_icv/D*((ww[kk])**2.)*dy[kk]
#                temporary_Gy[icv,icv] += b_icv/D*((ww[kk])**2.)*dx[kk] -1.*a_icv/D*((ww[kk])**2.)*dy[kk]
                
                temporary_Gy[icv,kk] = -1.*b_icv/D*((ww[kk])**2.)*dx[kk] + a_icv/D*((ww[kk])**2.)*dy[kk]
                
                www += 1
                    
            temporary_Gx[icv,icv] = np.sum(weights_Gx_pressure)
            temporary_Gy[icv,icv] = np.sum(weights_Gy_pressure)

##############################################################################
#   Interpolation for gradient operator (ncv,ncv) --> (nfa,ncv)              #
##############################################################################
            
        xy_fa_int = np.zeros((nfa_int,2))
        for uu in range(0, nfa_int):
            xy_fa_int[uu] = xy_fa[uu]
            itpl = csr_matrix((nfa_int, ncv))
            
        for mm in range(0, len(xy_fa)):
            if mm in face_top:
                pass # print "lid"
            elif mm in face_wall:
                pass # print "wall"
            elif mm in face_inlet:
                pass # print "inlet"
            elif mm in face_outlet:
                pass # print "outlet"
            else:
                two_cvs = cvofa[mm]
                cv1 = two_cvs[0]
                cv2 = two_cvs[1]
                    
                l_1 = math.hypot(xy_cv[cv1][0] - xy_fa[mm][0], xy_cv[cv1][1] - xy_fa[mm][1])
                l_2 = math.hypot(xy_cv[cv2][0] - xy_fa[mm][0], xy_cv[cv2][1] - xy_fa[mm][1])
                    
                # interpolation weights
                w1 = l_2/(l_1 + l_2)
                w2 = l_1/(l_1 + l_2)
                    
                itpl[mm, cv1] = w1
                itpl[mm, cv2] = w2
                        
        Gx = itpl.dot(temporary_Gx)
        Gy = itpl.dot(temporary_Gy)
                        
###############################################################################
#   Matrix A = Div dot Gradient                                               #
###############################################################################
                        
        A = Dx_f2cv.dot(Gx) + Dy_f2cv.dot(Gy)

###############################################################################
#   Iteration loop                                                            #
###############################################################################
        u_star = np.zeros((nno_int,2))
        iteration = input('Enter the number of iteration:')
        for n in range(iteration):
            print (n)

            

      
###############################################################################
#   U star (only for internal nodes)                                          #
###############################################################################
            
            
            #keyboard()
            u_star[:,0] = (np.identity(nno_int) + dt*(u_n[:,0]*Gx_conv+u_n[:,1]*Gy_conv) + dt*nu*B).dot(u_n[:,0]) + dt*u_n[:,0]*q_bc_dudx_conv + dt*u_n[:,1]*q_bc_dudy_conv + dt*nu*q_bc[:,0]

            u_star[:,1] = (np.identity(nno_int) + dt*(u_n[:,0]*Gx_conv+u_n[:,1]*Gy_conv) + dt*nu*B).dot(u_n[:,1]) + dt*u_n[:,0]*q_bc_dvdx_conv + dt*u_n[:,1]*q_bc_dvdy_conv + dt*nu*q_bc[:,1]
            
            #u_star = (np.identity(nno_int) + dt*(u_n[:,0]*Gx_conv+u_n[:,1]*Gy_conv)).dot(u_n) + dt*q_bc_conv
            #keyboard()
            
###############################################################################
#   Temperature                                                               #
###############################################################################
#            keyboard()

            # EE
            #phi_nn = np.identity(nno_int).dot(phi_n) - dt*(u_n[:,0]*(Gx_conv.dot(phi_n))+u_n[:,1]*(Gy_conv.dot(phi_n))) + dt*alpha*(B.dot(phi_n)) - dt*t_bc_conv +dt*alpha*t_bc
            
            # RK2

#            R_n = -1*(u_n[:,0]*(Gx_conv.dot(phi_n)) + u_n[:,1]*(Gy_conv.dot(phi_n))) + alpha*(B.dot(phi_n)) - t_bc_conv + alpha*t_bc
#            phi_1 = phi_n + dt*R_n
            
#           R_1 = -1*(u_n[:,0]*(Gx_conv.dot(phi_1)) + u_n[:,1]*(Gy_conv.dot(phi_1))) + alpha*(B.dot(phi_1)) - t_bc_conv + alpha*t_bc
            
#           phi_nn = phi_n + 1./2.*(R_n + R_1)*dt
            
            # RK4
            R_n = -1*(u_n[:,0]*(Gx_conv_t.dot(phi_n)) + u_n[:,1]*(Gy_conv_t.dot(phi_n))) + alpha*(B.dot(phi_n)) + -1*(u_n[:,0]*t_bc_x_conv + u_n[:,1]*t_bc_y_conv) + alpha*t_bc
            phi_1 = phi_n + dt/2*R_n
            R_1 = -1*(u_n[:,0]*(Gx_conv_t.dot(phi_1)) + u_n[:,1]*(Gy_conv_t.dot(phi_1))) + alpha*(B.dot(phi_1)) + -1*(u_n[:,0]*t_bc_x_conv + u_n[:,1]*t_bc_y_conv) + alpha*t_bc
            phi_2 = phi_n + dt/2*R_1
            R_2 = -1*(u_n[:,0]*(Gx_conv_t.dot(phi_2)) + u_n[:,1]*(Gy_conv_t.dot(phi_2))) + alpha*(B.dot(phi_2)) + -1*(u_n[:,0]*t_bc_x_conv + u_n[:,1]*t_bc_y_conv) + alpha*t_bc
            phi_3 = phi_n + dt*R_2
            R_3 = -1*(u_n[:,0]*(Gx_conv_t.dot(phi_3)) + u_n[:,1]*(Gy_conv_t.dot(phi_3))) + alpha*(B.dot(phi_3)) + -1*(u_n[:,0]*t_bc_x_conv + u_n[:,1]*t_bc_y_conv) + alpha*t_bc
            phi_nn = phi_n + dt/6.*(R_n + 4.*(1./2.*(R_1 + R_2))+R_3)


###############################################################################
#   Contour plot for u_star                                                   #
###############################################################################

            u_total_star = np.zeros((nno,2))
            for i in range(0,nno):
                if i in node_wall:
                    pass
                elif i in node_top:
                    u_total_star[i] = np.array([u_top[0], u_top[1]])
                elif i in node_wall:
                    u_total_star[i] = np.array([u_wall[0], u_wall[1]])
                elif i in node_inlet:
                    u_total_star[i] = np.array([u_inlet[0], u_inlet[1]])
                elif i in node_outlet:
                    u_total_star[i] = np.array([u_outlet[0], u_outlet[1]])
                else:
                    u_total_star[i] = u_star[i]
            # keyboard()
        
            u_magnitude_star = np.zeros(nno)
            for i in range(nno):
                u_magnitude_star[i] = np.linalg.norm(u_total_star[i])
            # keyboard()




##############################################################################
#   Interpolation of u_star on face centers                                  #
##############################################################################
            u_f_int = np.zeros((nfa_int,2))
            for i in range(0,nfa):
                #print i
                if i in face_wall:
                    #keyboard()
                    pass
                elif i in face_top:
                    pass
                elif i in face_inlet:
                    pass
                elif i in face_outlet:
                    pass
                else:
                    u_f_int[i] = np.array([np.mean(u_total_star[noofa[i],0]),np.mean(u_total_star[noofa[i],1])])

#keyboard()
##############################################################################
#   div * u_star                                                             #
##############################################################################


            Div_u_star = Dx_f2cv.dot(u_f_int[:,0]) + Dy_f2cv.dot(u_f_int[:,1]) + q_bc_div


##############################################################################
#   Pressure, P_n+1, Poisson equation                                        #
##############################################################################

            pressure = sp.sparse.linalg.spsolve(A, Div_u_star/dt)

##############################################################################
#   U_n+1 based on U_star and Pressure                                       #
##############################################################################
##############################################################################
#   1. Estimate the velocity at the nodal location by arithmetic average     #
##############################################################################

            u_nn_face = np.zeros((nfa_int,2))
            u_nn_face[:,0] = u_f_int[:,0] - dt*Gx*pressure
            u_nn_face[:,1] = u_f_int[:,1] - dt*Gy*pressure

            x_fa_int = np.zeros(nfa_int) # the position of internal faces
            y_fa_int = np.zeros(nfa_int)
            for yyy in range(0, nfa_int):
                x_fa_int[yyy] = xy_fa[yyy,0]
                y_fa_int[yyy] = xy_fa[yyy,1]
        
##############################################################################
#   Re-calculation from face to nodal locations                              #
##############################################################################

            u_nn_node = np.zeros((nno_int,2))
            for ino in range(0,nno_int):
                u_nn_node[ino,0] = np.mean(u_nn_face[faono[ino],0])
                u_nn_node[ino,1] = np.mean(u_nn_face[faono[ino],1])
        
##############################################################################
#   Update initial u_n <--u_nn_node // temperature                           #
##############################################################################
            judge = np.zeros(nno_int)
            for yy in range(nno_int):
                judge[yy] = u_n[yy].dot(u_nn_node[yy])
            
            if n > iteration: #any(s<=0 for s in judge): # n > iteration:
##############################################################################
#   Gradient operator (nno_int x nno int) for convective term                #
#   for energy equation  "Upwind scheme"                                     #
##############################################################################
                print "Upwind scheme is applied to convective term"
    
    
                for ino in range(nno_int):
                    sno = np.unique(noofa[faono[ino]]) # neighboring vertices including centroid
                    esno = np.unique(noofa[np.unique(np.concatenate(faono[sno], axis=0))])
        
                    index = np.where(esno == ino)
                    only_sno = np.delete(esno, index) # only neighboring CVs
            
                    for tt in only_sno:
                        vector_1 = xy_no[ino] - xy_no[tt]
                        inner_dot = vector_1[0]*u_nn_node[ino][0]+vector_1[1]*u_nn_node[ino][1]
                        if inner_dot >=0:
                            pass
                        else:
                            index2 = np.where(only_sno == tt)
                            only_sno = np.delete(only_sno, index2)
            
                
                
                    a_ino = 0.0
                    b_ino = 0.0
                    c_ino = 0.0
                    
                    dx = np.zeros(nno)
                    dy = np.zeros(nno)
                    ww = np.zeros(nno)
                    
                    k = 0
                    for xx in only_sno:
                        dx[xx] = xy_no[xx,0] - xy_no[ino,0]
                        dy[xx] = xy_no[xx,1] - xy_no[ino,1]
                        ww[xx] = 1./np.sqrt(dx[xx]**2. + dy[xx]**2.)
        
                    for xxx in only_sno:
                        a_ino += ((ww[xxx])**2.)*(dx[xxx])**2.
                        b_ino += ((ww[xxx])**2.)*(dx[xxx])*(dy[xxx])
                        c_ino += ((ww[xxx])**2.)*(dy[xxx])**2.
                    D = a_ino*c_ino-(b_ino)**2.
                    if D==0:
                        keyboard()
                    
                    weights_Gx_conv_t = np.zeros(len(only_sno)) # 0.0
                    weights_Gy_conv_t = np.zeros(len(only_sno)) # 0.0
                    w = 0
                    #keyboard()
                    for kk in only_sno:
                        weights_Gx_conv_t[w] = -1.*c_ino/D*((ww[kk])**2.)*dx[kk] + b_ino/D*((ww[kk])**2.)*dy[kk]
                        
                        temporary_Gx_conv_t[ino,kk] = c_ino/D*((ww[kk])**2.)*dx[kk] - b_ino/D*((ww[kk])**2.)*dy[kk]
                        
                        weights_Gy_conv_t[w] = b_ino/D*((ww[kk])**2.)*dx[kk] -1.*a_ino/D*((ww[kk])**2.)*dy[kk]
                        
                        temporary_Gy_conv_t[ino,kk] = -1.*b_ino/D*((ww[kk])**2.)*dx[kk] + a_ino/D*((ww[kk])**2.)*dy[kk]
                        
                        w += 1
                    
                    temporary_Gx_conv_t[ino,ino] = np.sum(weights_Gx_conv_t)
                    temporary_Gy_conv_t[ino,ino] = np.sum(weights_Gy_conv_t)
                
                for r in range(0, nno_int):
                    Gx_conv_t[:,r] = temporary_Gx_conv_t[:,r]
                    Gy_conv_t[:,r] = temporary_Gy_conv_t[:,r]

                for ino in range(0,nno_int):
                    for qq in node_wall:
                        t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_wall
                        t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_wall
                   
                    for qq in node_top:
                        t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_top
                        t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_top

                    for qq in node_inlet:
                        t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_inlet
                        t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_inlet

                    for qq in node_outlet:
                        t_bc_x_conv[ino] += temporary_Gx_conv_t[ino,qq]*t_outlet
                        t_bc_y_conv[ino] += temporary_Gy_conv_t[ino,qq]*t_outlet
                            


### Update values ###

            u_n = u_nn_node
            phi_n = phi_nn
                    
           

##############################################################################
#   Post-processing                                                          #
##############################################################################

        if Problem1_1:
            
            figwidth       = 18
            figheight      = 6
            lineWidth      = 3
            textFontSize   = 28
            gcafontSize    = 30

            plt.title('Arrows scale with plot width, not view')
            plt.quiver(x_fa_int, y_fa_int, u_nn_face[:,0], u_nn_face[:,1], units='width', color='r')
    
    
            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_QUIVER PLOT_' + 'iteration='+ str(iteration) + '.jpg' # change file name
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
        
        
            total_u_n = np.zeros((nno,2))
            for i in range(nno):
                if i in node_top:
                    total_u_n[i] = np.array([u_top[0], u_top[1]])
                elif i in node_wall:
                    total_u_n[i] = np.array([u_wall[0], u_wall[1]])
                elif i in node_inlet:
                    total_u_n[i] = np.array([u_inlet[0], u_inlet[1]])
                elif i in node_outlet:
                    total_u_n[i] = np.array([u_outlet[0], u_outlet[1]])
                else:
                    total_u_n[i] = u_n[i]
                        #keyboard()
            magnitude_u_n = np.zeros(nno)
            for i in range(nno):
                magnitude_u_n[i] = np.linalg.norm(total_u_n[i])
            
            
            total_t = np.zeros(nno)
            for i in range(nno):
                if i in node_top:
                    total_t[i] = t_top
                elif i in node_wall:
                    total_t[i] = t_wall
                elif i in node_inlet:
                    total_t[i] = t_inlet
                elif i in node_outlet:
                    total_t[i] = t_outlet
                else:
                    total_t[i] = phi_nn[i]
            

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = magnitude_u_n # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
        
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(np.isnan(Z),Z)
        
            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_FLOW FIELD CONTOUR_' + 'iteration='+ str(iteration)+'.jpg'  # setting up file name
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=1.0, vmin=0.)
            plt.colorbar()
            ax.set_title('FLOW CONTOUR')
        
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()



            # Pressure
            # contour
            x = xy_cv[:,0] # x coordinates of data
            y = xy_cv[:,1] # y coordinates of data
            phi = pressure # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)
        
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(np.isnan(Z),Z)
            
            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_PRESSURE FIELD CONTOUR_' + 'iteration='+ str(iteration)+'.jpg'  # setting up file name
            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud', vmax=2.0, vmin=-2.)
            plt.colorbar()
            ax.set_title('PRESSURE CONTOUR')
            
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

   

            # X and Y velocity plot
            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            ZZy = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                          np.vstack(total_u_n[:,1].flatten()),(X,Y),method='cubic').reshape(X.shape)

            ZZZy[graph] = ZZy[200]

            plt.gca().set_color_cycle(['red','green','blue'])
            plt.plot(X[200], ZZZy[0])
            plt.plot(X[200], ZZZy[1])
            plt.plot(X[200], ZZZy[2])
            plt.axis([0, 1, -1, 1.])
            plt.xlabel('X')
            plt.ylabel('Y-VELOCITY')
            plt.title('Y-VELOCITY ALONG THE HORIZONTAL LINE')
            
            plt.legend(['HW4'], loc='upper right')


            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_Y-VELOCITY.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()


            ZZx = griddata(np.vstack((x.flatten(),y.flatten())).T, \
              np.vstack(total_u_n[:,0].flatten()),(X,Y),method='cubic').reshape(X.shape)
              #keyboard()
            ZZZx[graph] = ZZx[:,200]

            plt.gca().set_color_cycle(['red','green','blue'])
            plt.plot(ZZZx[0], Y[:,1])
            plt.plot(ZZZx[1], Y[:,1])
            plt.plot(ZZZx[2], Y[:,1])
            plt.axis([-1, 1, 0., 1.])
            plt.xlabel('X-VELOCITY')
            plt.ylabel('Y')
            plt.title('X-VELOCITY ALONG THE VERTICAL LINE')
            
            plt.legend(['HW4'], loc='upper right')

    
            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_X-VELOCITY.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
            
            # Temperature along vertical line
            
            ZZt = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                np.vstack(total_t.flatten()),(X,Y),method='cubic').reshape(X.shape)
            
            ZZZt[graph] = ZZt[:,200]
            graph +=1

            plt.gca().set_color_cycle(['red','green','blue'])
            plt.plot(X[200], ZZZt[0])
            plt.plot(X[200], ZZZt[0])
            plt.plot(X[200], ZZZt[0])
            plt.axis([0,1,250,350])
            plt.xlabel('TEMPERATURE (K)')
            plt.ylabel('Y')
            plt.title('TEMPERATURE ALONG THE VERTICAL LINE')
            
            plt.legend(['ncv=488','ncv=2269','ncv=7394'], loc='upper right')
            
            
            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_TEMP.jpg'
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()
            
        


            final_phi = np.zeros(len(xy_no))
            for nn in range(0, len(xy_no)):
                if nn in node_wall:
                    final_phi[nn] = t_wall
                elif nn in node_top:
                    final_phi[nn] = t_top
                elif nn in node_inlet:
                    final_phi[nn] = t_inlet
                elif nn in node_outlet:
                    final_phi[nn] = t_outlet
                else:
                    final_phi[nn] = phi_nn[nn]
                    
            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi = final_phi # values of data
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
            # interpolate Z values on defined grid
            Z = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                                 np.vstack(phi.flatten()),(X,Y),method='cubic').reshape(X.shape)

                            
            # mask nan values, so they will not appear on plot
            Zm = np.ma.masked_where(np.isnan(Z),Z)
                                
            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_TEMPERATURE FIELD CONTOUR_' + 'iteration='+ str(iteration)+'.jpg'  # setting up file name

            # plot
            fig = plt.figure(0,figsize=(figwidth,figheight))
            ax = fig.add_subplot(111, aspect='equal')
            plt.axes(ax)
            plt.pcolormesh(X,Y,Zm,shading='gouraud',vmax=350, vmin=250)  # vmax=350, vmin=250
            plt.colorbar()
            ax.set_title('TEMPERATURE CONTOUR')
    
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()

            # contour
            x = xy_no[:,0] # x coordinates of data
            y = xy_no[:,1] # y coordinates of data
            phi1 = total_u_n[:,0] # values of data
            phi2 = total_u_n[:,1] # values of data
    
            # define regular grid spatially covering input data
            n = 400
            xg = np.linspace(x.min(),x.max(),n)
            yg = np.linspace(y.min(),y.max(),n)
            X,Y = np.meshgrid(xg,yg)
        
            # interpolate Z values on defined grid
            U = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                     np.vstack(phi1.flatten()),(X,Y),method='cubic').reshape(X.shape)
            V = griddata(np.vstack((x.flatten(),y.flatten())).T, \
                         np.vstack(phi2.flatten()),(X,Y),method='cubic').reshape(X.shape)
            

            figure_folder = "../report/"
            figure_name = filename.split('.')[0] + '_STREAMLINE CONTOUR_' + 'iteration='+ str(iteration)+'.jpg'  # setting up file name
                         
             # plot
            plt.figure(0, figsize = (10,10))
            plt.streamplot(X, Y, U, V, color='k', linewidth=2, density=3)
            ax.set_title('STREAMLINE CONTOUR')
                 
            figure_file_path = figure_folder + figure_name
            print "Saving figure: " + figure_file_path
            plt.tight_layout()
            plt.savefig(figure_file_path)
            plt.close()




keyboard()
